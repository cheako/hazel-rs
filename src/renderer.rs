// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::reexport::vk_helper::*;

pub mod base;
mod image;
mod pipeline;
pub mod r2d;
mod render;
pub mod render_target;

pub use self::image::Renderer;
pub use crate::reexport::ash::vk::{
    DescriptorSetLayoutBinding, Format, PipelineLayoutCreateInfo, ShaderStageFlags,
    VertexInputAttributeDescription,
};
pub use crate::reexport::vk_helper::PipelineLayout;
pub use base::{PipelineVariableValue, RendererViewport, UserDataTrait};
pub use pipeline::{Result as PipelineResult, ShaderStageInput, ShaderStagesInput};

pub use r2d::{Renderer as Renderer2D, *};
pub use render::*;

mod buffer {
    use std::fmt::Debug;
    use std::ops::Deref;

    use ash_tray::vk_helper::CommandBuffer;

    use super::{UserDataTrait, VkHelperUser};
    use crate::reexport::ash::vk;
    use crate::reexport::vk_helper::Buffer;

    #[derive(Clone, Debug)]
    pub struct VertexBuffer<T: UserDataTrait> {
        pub inner: Buffer<VkHelperUser<T>>,
        pub stride: u32,
    }

    impl<T: UserDataTrait> Deref for VertexBuffer<T> {
        type Target = Buffer<VkHelperUser<T>>;

        fn deref(&self) -> &Self::Target {
            &self.inner
        }
    }

    #[derive(Clone, Debug)]
    pub struct IndexBuffer<T: UserDataTrait> {
        pub inner: Buffer<VkHelperUser<T>>,
        pub count: u32,
    }

    impl<T: UserDataTrait> IndexBuffer<T> {
        pub fn bind(&self, command_buffer: &CommandBuffer<VkHelperUser<T>>) {
            command_buffer.bind_index_buffer((**self).clone(), 0, vk::IndexType::UINT32);
        }
    }

    impl<T: UserDataTrait> Deref for IndexBuffer<T> {
        type Target = Buffer<VkHelperUser<T>>;

        fn deref(&self) -> &Self::Target {
            &self.inner
        }
    }

    #[derive(Clone, Debug)]
    pub struct UniformBuffer<T: UserDataTrait> {
        pub inner: Buffer<VkHelperUser<T>>,
        pub binding: vk::DescriptorSetLayoutBindingBuilder,
    }

    impl<T: UserDataTrait> UniformBuffer<T> {
        pub fn binding(&mut self, binding: vk::DescriptorSetLayoutBindingBuilder) -> &mut Self {
            self.binding = binding;
            self
        }
    }

    impl<T: UserDataTrait> Deref for UniformBuffer<T> {
        type Target = Buffer<VkHelperUser<T>>;

        fn deref(&self) -> &Self::Target {
            &self.inner
        }
    }
}

pub use buffer::*;
