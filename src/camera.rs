// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use ash_tray::vk_helper::CommandBuffer;

use crate::event::VirtualKeyCode;
use crate::reexport::winit::dpi;
use crate::renderer::VkHelperUser;

pub struct KeyMap {
    pub left: VirtualKeyCode,
    pub right: VirtualKeyCode,
    pub up: VirtualKeyCode,
    pub down: VirtualKeyCode,
    pub clockwise: Option<(VirtualKeyCode, VirtualKeyCode)>,
}

pub struct Layer {
    pub camera: crate::maths::camera::Camera,
    pub aspect_ratio: f32,
    pub key_map: KeyMap,
    pub zoom_level: f32,
    pub translation_speed: f32,
    pub rotation_speed: f32,
}

impl Layer {
    pub fn new(aspect_ratio: f32, key_map: KeyMap) -> Self {
        Self {
            camera: crate::maths::camera::Camera::new(-aspect_ratio, aspect_ratio),
            aspect_ratio,
            key_map,
            zoom_level: 1.,
            translation_speed: 5.,
            rotation_speed: 180.,
        }
    }
}

impl<UserEvent: 'static, UserData: super::renderer::UserDataTrait> crate::Layer<UserEvent, UserData>
    for Layer
{
    fn get_name(&mut self) -> String {
        "Hazel Orthographic Camera Controller".to_owned()
    }

    fn on_preupdate(
        &mut self,
        api: &crate::AppAPI<UserEvent, UserData>,
        _: &CommandBuffer<VkHelperUser<UserData>>,
        _: &crate::EventLoopWindowTarget<UserEvent>,
        _: &mut crate::ControlFlow,
    ) {
        let elspesd = api.elapsed.get().as_micros() as f32 / 1_000_000.;
        let Self {
            translation_speed,
            rotation_speed,
            ..
        } = *self;
        if api.get_key(self.key_map.left) {
            self.camera.delta_x(translation_speed * elspesd);
        } else if api.get_key(self.key_map.right) {
            self.camera.delta_x(translation_speed * -elspesd);
        };

        if api.get_key(self.key_map.up) {
            self.camera.delta_y(translation_speed * elspesd);
        } else if api.get_key(self.key_map.down) {
            self.camera.delta_y(translation_speed * -elspesd);
        };

        let clockwise = self.key_map.clockwise;
        clockwise.iter().for_each(|&(cc, c)| {
            if api.get_key(cc) {
                self.camera.delta_rotation(rotation_speed * elspesd);
            } else if api.get_key(c) {
                self.camera.delta_rotation(rotation_speed * -elspesd);
            }
        });
    }

    fn on_event(
        &mut self,
        _: &crate::AppAPI<UserEvent, UserData>,
        e: &crate::event::Event<UserEvent>,
        _: &crate::EventLoopWindowTarget<UserEvent>,
        _: &mut crate::ControlFlow,
    ) -> bool {
        use crate::event::Event::WindowEvent;
        use crate::event::{MouseScrollDelta::*, WindowEvent::*};
        match &e {
            WindowEvent {
                event:
                    MouseWheel {
                        delta: LineDelta(_, y),
                        ..
                    },
                ..
            } => {
                self.zoom_level -= y / 4.;
                self.camera.resize(
                    -self.aspect_ratio * self.zoom_level,
                    self.aspect_ratio * self.zoom_level,
                    -self.zoom_level,
                    self.zoom_level,
                );
                self.translation_speed = self.zoom_level;
            }
            WindowEvent {
                event:
                    MouseWheel {
                        delta: PixelDelta(dpi::PhysicalPosition { y, .. }),
                        ..
                    },
                ..
            } => {
                self.zoom_level -= (y / f64::from(4 * 24)) as f32;
                self.camera.resize(
                    -self.aspect_ratio * self.zoom_level,
                    self.aspect_ratio * self.zoom_level,
                    -self.zoom_level,
                    self.zoom_level,
                );
                self.translation_speed = self.zoom_level;
            }
            WindowEvent {
                event: Resized(dpi::PhysicalSize { width, height }),
                ..
            } => {
                self.aspect_ratio = (width / height) as f32;
                self.camera.resize(
                    -self.aspect_ratio * self.zoom_level,
                    self.aspect_ratio * self.zoom_level,
                    -self.zoom_level,
                    self.zoom_level,
                );
            }
            _ => {}
        };
        false
    }
}
