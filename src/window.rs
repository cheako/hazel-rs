// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use winit::event_loop::EventLoopBuilder;

use crate::reexport::winit::{dpi::LogicalSize, window::WindowBuilder as winit_window_builder};
pub use crate::reexport::winit::{event_loop::EventLoop, window::Window as WinitWindow};

pub enum Window<T: 'static> {
    Full(Box<StructWindow<T>>),
    Half(WindowBuilderWithWinitWindow),
}

impl<T: 'static> Window<T> {
    pub fn get_window(&self) -> &WinitWindow {
        use Window::*;

        match self {
            Full(w) => &w.winit_window,
            Half(w) => &w.winit_window,
        }
    }

    pub fn get_window_mut(&mut self) -> &mut WinitWindow {
        use Window::*;

        match self {
            Full(w) => &mut w.winit_window,
            Half(w) => &mut w.winit_window,
        }
    }
}

pub struct StructWindow<T: 'static> {
    pub event_loop: EventLoop<T>,
    pub winit_window: WinitWindow,
}

impl<T: 'static> Default for Window<T> {
    fn default() -> Self {
        let window_builder: WindowBuilderWithEventLoop<T> = Default::default();
        window_builder.build()
    }
}

pub struct WindowBuilderWithWinitWindow {
    pub winit_window: WinitWindow,
}

impl WindowBuilderWithWinitWindow {
    fn new<T: 'static>(event_loop: &mut EventLoop<T>) -> Self {
        let logical_size = LogicalSize::new(800_f64, 600_f64);
        let winit_window = winit_window_builder::new()
            .with_title("Hazel-rs")
            .with_inner_size(logical_size)
            .with_visible(false)
            .build(event_loop)
            .unwrap();
        WindowBuilderWithWinitWindow { winit_window }
    }

    pub fn build<T: 'static>(self) -> Window<T> {
        Window::Full(Box::new(StructWindow {
            event_loop: EventLoopBuilder::<T>::with_user_event().build(),
            winit_window: self.winit_window,
        }))
    }

    pub fn set_event_loop<T: 'static>(self, event_loop: EventLoop<T>) -> Window<T> {
        Window::Full(Box::new(StructWindow {
            event_loop,
            winit_window: self.winit_window,
        }))
    }
}

pub struct WindowBuilderWithEventLoop<T: 'static> {
    pub event_loop: EventLoop<T>,
}

impl<T: 'static> Default for WindowBuilderWithEventLoop<T> {
    fn default() -> Self {
        Self {
            event_loop: EventLoopBuilder::<T>::with_user_event().build(),
        }
    }
}

impl<T: 'static> WindowBuilderWithEventLoop<T> {
    pub fn build(mut self) -> Window<T> {
        WindowBuilderWithWinitWindow::new(&mut self.event_loop).set_event_loop(self.event_loop)
    }

    pub fn set_winit_window(self, winit_window: WinitWindow) -> Window<T> {
        Window::Full(Box::new(StructWindow {
            event_loop: self.event_loop,
            winit_window,
        }))
    }
}
