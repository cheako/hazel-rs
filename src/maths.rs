// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pub mod camera {
    use crate::reexport::glm::{self, Mat4, Vec3};

    pub struct Camera {
        pub projection: Mat4,
        pub view_projection: Option<Mat4>,
        pub position: Vec3,
        pub direction: f32,
    }

    impl Default for Camera {
        fn default() -> Self {
            Self {
                projection: glm::ortho(-1., 1., 1., -1., 1., -1.),
                view_projection: None,
                position: glm::vec3(0f32, 0., 0.),
                direction: 0.,
            }
        }
    }

    impl Camera {
        pub fn new(left: f32, right: f32) -> Self {
            Self {
                projection: glm::ortho(left, right, 1., -1., 1., -1.),
                ..Default::default()
            }
        }

        pub fn resize(&mut self, left: f32, right: f32, top: f32, bottom: f32) {
            self.view_projection = None;
            self.projection = glm::ortho(left, right, bottom, top, 1., -1.);
        }

        pub fn delta_x(&mut self, delta: f32) {
            self.view_projection = None;
            self.position += glm::vec3(
                self.direction.cos() * delta,
                self.direction.sin() * delta,
                0.,
            );
        }

        pub fn delta_y(&mut self, delta: f32) {
            self.view_projection = None;
            self.position += glm::vec3(
                self.direction.sin() * delta,
                self.direction.cos() * delta,
                0.,
            );
        }

        pub fn delta_rotation(&mut self, delta: f32) {
            self.view_projection = None;
            self.direction += delta.to_radians();
        }

        pub fn get_view_projection(&mut self) -> &Mat4 {
            let Self {
                projection,
                position,
                direction,
                ..
            } = self;
            &*self.view_projection.get_or_insert_with(move || {
                glm::translate(
                    &glm::rotation(*direction, &glm::vec3(0f32, 0., 1.)),
                    position,
                ) * *projection
            })
        }
    }
}
