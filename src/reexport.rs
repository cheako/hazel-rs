// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pub mod ash_tray {
    pub use ::ash_tray::*;
    pub mod ash {
        pub use ::ash_tray::ash::*;
        pub mod vk {
            pub use ::ash_tray::ash::vk::*;
            pub use ::ash_tray::ash::vk::{
                DescriptorSetLayoutBinding as _DescriptorSetLayoutBinding,
                DescriptorSetLayoutBindingBuilder as _DescriptorSetLayoutBindingBuilder,
            };

            #[repr(C)]
            #[derive(Copy, Clone, Debug)]
            #[doc = "<https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkDescriptorSetLayoutBinding.html>"]
            pub struct DescriptorSetLayoutBinding {
                pub binding: u32,
                pub descriptor_type: DescriptorType,
                pub descriptor_count: u32,
                pub stage_flags: ShaderStageFlags,
                pub p_immutable_samplers: *const Sampler,
            }
            impl ::std::default::Default for DescriptorSetLayoutBinding {
                fn default() -> DescriptorSetLayoutBinding {
                    DescriptorSetLayoutBinding {
                        binding: u32::default(),
                        descriptor_type: DescriptorType::default(),
                        descriptor_count: u32::default(),
                        stage_flags: ShaderStageFlags::default(),
                        p_immutable_samplers: ::std::ptr::null(),
                    }
                }
            }
            impl DescriptorSetLayoutBinding {
                pub fn builder() -> DescriptorSetLayoutBindingBuilder {
                    DescriptorSetLayoutBindingBuilder {
                        inner: DescriptorSetLayoutBinding::default(),
                    }
                }
            }
            #[repr(transparent)]
            #[derive(Copy, Clone, Debug)]
            pub struct DescriptorSetLayoutBindingBuilder {
                inner: DescriptorSetLayoutBinding,
            }
            impl ::std::ops::Deref for DescriptorSetLayoutBindingBuilder {
                type Target = DescriptorSetLayoutBinding;
                fn deref(&self) -> &Self::Target {
                    &self.inner
                }
            }
            impl ::std::ops::DerefMut for DescriptorSetLayoutBindingBuilder {
                fn deref_mut(&mut self) -> &mut Self::Target {
                    &mut self.inner
                }
            }
            impl DescriptorSetLayoutBindingBuilder {
                pub fn binding(mut self, binding: u32) -> DescriptorSetLayoutBindingBuilder {
                    self.inner.binding = binding;
                    self
                }
                pub fn descriptor_type(
                    mut self,
                    descriptor_type: DescriptorType,
                ) -> DescriptorSetLayoutBindingBuilder {
                    self.inner.descriptor_type = descriptor_type;
                    self
                }
                pub fn descriptor_count(
                    mut self,
                    descriptor_count: u32,
                ) -> DescriptorSetLayoutBindingBuilder {
                    self.inner.descriptor_count = descriptor_count;
                    self
                }
                pub fn stage_flags(
                    mut self,
                    stage_flags: ShaderStageFlags,
                ) -> DescriptorSetLayoutBindingBuilder {
                    self.inner.stage_flags = stage_flags;
                    self
                }
                #[doc = r" Calling build will **discard** all the lifetime information. Only call this if"]
                #[doc = r" necessary! Builders implement `Deref` targeting their corresponding Vulkan struct,"]
                #[doc = r" so references to builders can be passed directly to Vulkan functions."]
                pub fn build(self) -> DescriptorSetLayoutBinding {
                    self.inner
                }
            }
        }
    }
}
pub use self::ash_tray::ash;
pub use self::ash_tray::imgui;
pub use self::ash_tray::imguiash;
pub use self::ash_tray::vk_helper;
pub use self::ash_tray::vk_mem;
pub use self::ash_tray::winit;
pub use clipboard;
pub use nalgebra_glm as glm;
pub use nalgebra_glm;
pub use slog;
pub use slog_async;
pub use slog_term;
