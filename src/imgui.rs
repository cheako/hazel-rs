// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::cell::RefCell;
use std::marker::PhantomData;
use std::rc::Rc;
use std::time::Duration;

use crate::reexport::ash_tray::vk_helper::CommandBuffer;
use crate::reexport::clipboard::{ClipboardContext, ClipboardProvider};
pub use crate::reexport::imgui::{
    self, BackendFlags as ImBackendFlags, ClipboardBackend, Context as ImGui, ImStr, ImString, Io,
    Key, Ui,
};
use crate::reexport::imguiash::Renderer;
use crate::reexport::winit::{
    self,
    dpi::{LogicalPosition, PhysicalPosition},
};
use crate::renderer::{UserDataTrait, VkHelperUser};

pub trait Interface {
    fn get_name(&mut self) -> String {
        "".to_owned()
    }

    fn on_update(&mut self, ui: &Ui) {
        let mut demo = true;
        ui.show_demo_window(&mut demo);
    }
}

pub struct Layer<UserEvent: 'static, UserData: UserDataTrait> {
    pub imgui: RefCell<ImGui>,
    pub renderer: Renderer<VkHelperUser<UserData>>,
    pub last_frame: Duration,
    pub has_focus: bool,
    pub current_cursor: Option<imgui::MouseCursor>,
    pub inner: Rc<RefCell<dyn Interface>>,
    phantom: PhantomData<UserEvent>,
}

struct HazelClipboardBackend(ClipboardContext);

impl Default for HazelClipboardBackend {
    fn default() -> Self {
        Self(ClipboardProvider::new().unwrap())
    }
}

impl ClipboardBackend for HazelClipboardBackend {
    fn get(&mut self) -> Option<String> {
        self.0
            .get_contents()
            .inspect_err(|e| {
                dbg!(e.to_string());
            })
            .ok()
    }

    fn set(&mut self, value: &str) {
        self.0.set_contents(value.to_owned()).unwrap();
    }
}

impl<UserEvent: 'static, UserData: UserDataTrait> Layer<UserEvent, UserData> {
    pub fn new(
        api: &crate::AppAPI<UserEvent, UserData>,
        command_buffer: &CommandBuffer<VkHelperUser<UserData>>,
        inner: Rc<RefCell<dyn Interface>>,
    ) -> Self {
        let imgui = RefCell::new(ImGui::create());
        let scale_factor = api.renderer.window.scale_factor().recip() as f32;

        imgui
            .borrow_mut()
            .set_clipboard_backend(HazelClipboardBackend::default());

        let renderer = Renderer::new(
            api.renderer.get_device().clone(),
            api.renderer.allocator.clone(),
            None,
            command_buffer,
            &mut imgui.borrow_mut(),
        )
        .unwrap();

        renderer
            .set_render_pass(
                api.renderer
                    .viewport
                    .borrow()
                    .as_ref()
                    .unwrap()
                    .get_render_pass()
                    .clone(),
            )
            .unwrap();

        {
            use crate::event::VirtualKeyCode;
            let logical_size = api.renderer.window.inner_size();

            let mut imgui = imgui.borrow_mut();
            let style = imgui.style_mut();
            style.use_dark_colors();
            let io = imgui.io_mut();
            io.config_flags |= imgui::ConfigFlags::DOCKING_ENABLE;
            io.display_framebuffer_scale = [scale_factor, scale_factor];
            io.display_size = [logical_size.width as f32, logical_size.height as f32];
            io.backend_flags.insert(ImBackendFlags::HAS_MOUSE_CURSORS);
            io.backend_flags.insert(ImBackendFlags::HAS_SET_MOUSE_POS);
            io[Key::Tab] = VirtualKeyCode::Tab as _;
            io[Key::LeftArrow] = VirtualKeyCode::Left as _;
            io[Key::RightArrow] = VirtualKeyCode::Right as _;
            io[Key::UpArrow] = VirtualKeyCode::Up as _;
            io[Key::DownArrow] = VirtualKeyCode::Down as _;
            io[Key::PageUp] = VirtualKeyCode::PageUp as _;
            io[Key::PageDown] = VirtualKeyCode::PageDown as _;
            io[Key::Home] = VirtualKeyCode::Home as _;
            io[Key::End] = VirtualKeyCode::End as _;
            io[Key::Insert] = VirtualKeyCode::Insert as _;
            io[Key::Delete] = VirtualKeyCode::Delete as _;
            io[Key::Backspace] = VirtualKeyCode::Back as _;
            io[Key::Space] = VirtualKeyCode::Space as _;
            io[Key::Enter] = VirtualKeyCode::Return as _;
            io[Key::Escape] = VirtualKeyCode::Escape as _;
            io[Key::A] = VirtualKeyCode::A as _;
            io[Key::C] = VirtualKeyCode::C as _;
            io[Key::V] = VirtualKeyCode::V as _;
            io[Key::X] = VirtualKeyCode::X as _;
            io[Key::Y] = VirtualKeyCode::Y as _;
            io[Key::Z] = VirtualKeyCode::Z as _;

            imgui.set_platform_name(Some(format!("imgui-hazel {}", env!("CARGO_PKG_VERSION"))));
        }

        Self {
            imgui,
            renderer,
            last_frame: Duration::from_nanos(1),
            has_focus: false,
            current_cursor: None,
            inner,
            phantom: Default::default(),
        }
    }
}

impl<UserEvent: 'static, UserData: UserDataTrait> crate::Layer<UserEvent, UserData>
    for Layer<UserEvent, UserData>
{
    fn get_name(&mut self) -> String {
        self.inner.borrow_mut().get_name()
    }

    fn on_preupdate(
        &mut self,
        _: &crate::AppAPI<UserEvent, UserData>,
        _: &CommandBuffer<VkHelperUser<UserData>>,
        _: &crate::EventLoopWindowTarget<UserEvent>,
        _: &mut crate::ControlFlow,
    ) {
        self.renderer.begin_frame();
    }

    fn on_update(
        &mut self,
        api: &crate::AppAPI<UserEvent, UserData>,
        command_buffer: &CommandBuffer<VkHelperUser<UserData>>,
        _: &crate::EventLoopWindowTarget<UserEvent>,
        _: &mut crate::ControlFlow,
    ) {
        let mut imgui = self.imgui.borrow_mut();
        let io = imgui.io_mut();

        io.update_delta_time(self.last_frame);

        if io.want_set_mouse_pos {
            let logical_pos =
                LogicalPosition::new(f64::from(io.mouse_pos[0]), f64::from(io.mouse_pos[1]));
            api.renderer
                .window
                .set_cursor_position(logical_pos)
                .unwrap();
        }

        let io_mouse_cursor_change = !io
            .config_flags
            .contains(imgui::ConfigFlags::NO_MOUSE_CURSOR_CHANGE);
        let io_mouse_draw_cursor = io.mouse_draw_cursor;

        let ui = imgui.frame();

        let mouse_cursor = ui.mouse_cursor();
        if io_mouse_cursor_change && self.current_cursor != mouse_cursor {
            use winit::window::CursorIcon as MouseCursor;

            /* Todo: simplify? */
            match mouse_cursor {
                Some(mouse_cursor) if !io_mouse_draw_cursor => {
                    api.renderer.window.set_cursor_visible(true);
                    api.renderer.window.set_cursor_icon(match mouse_cursor {
                        imgui::MouseCursor::Arrow => MouseCursor::Arrow,
                        imgui::MouseCursor::TextInput => MouseCursor::Text,
                        imgui::MouseCursor::ResizeAll => MouseCursor::Move,
                        imgui::MouseCursor::ResizeNS => MouseCursor::NsResize,
                        imgui::MouseCursor::ResizeEW => MouseCursor::EwResize,
                        imgui::MouseCursor::ResizeNESW => MouseCursor::NeswResize,
                        imgui::MouseCursor::ResizeNWSE => MouseCursor::NwseResize,
                        imgui::MouseCursor::Hand => MouseCursor::Hand,
                        imgui::MouseCursor::NotAllowed => MouseCursor::NotAllowed,
                    });
                }
                _ => api.renderer.window.set_cursor_visible(false),
            }
            self.current_cursor = mouse_cursor;
        }

        // unsafe {
        //    ash_tray::imgui_sys::igDockSpaceOverViewport(ui, Default::default(), Default::default())
        // };
        self.inner.borrow_mut().on_update(ui);
        let layers = api.layers.borrow().clone();
        layers.iter().for_each(|it| {
            it.iter().for_each(|it| {
                if let Ok(mut it) = it.try_borrow_mut() {
                    it.on_imgui_update(ui)
                }
            })
        });

        self.renderer.render(&mut imgui, command_buffer).unwrap();
    }

    fn on_event(
        &mut self,
        api: &crate::AppAPI<UserEvent, UserData>,
        e: &crate::event::Event<UserEvent>,
        _: &crate::EventLoopWindowTarget<UserEvent>,
        _: &mut crate::ControlFlow,
    ) -> bool {
        use crate::event::Event::*;

        let mut imgui = self.imgui.borrow_mut();
        let io = imgui.io_mut();
        match &e {
            WindowEvent { event, .. } => {
                use crate::event::{ElementState::*, KeyboardInput as KeyInput, WindowEvent::*};

                match event {
                    Resized(_) => {
                        self.renderer
                            .set_render_pass(
                                api.renderer
                                    .viewport
                                    .borrow()
                                    .as_ref()
                                    .unwrap()
                                    .get_render_pass()
                                    .clone(),
                            )
                            .unwrap();
                    }
                    Moved(_) => {}
                    CloseRequested => {}
                    Destroyed => {}
                    DroppedFile(_) => {}
                    HoveredFile(_) => {}
                    HoveredFileCancelled => {}
                    ReceivedCharacter(character) => {
                        io.add_input_character(*character);
                    }
                    Focused(x) => {
                        self.has_focus = *x;
                    }
                    KeyboardInput {
                        input:
                            KeyInput {
                                state,
                                virtual_keycode: Some(virtual_keycode),
                                ..
                            },
                        ..
                    } => {
                        io.keys_down[*virtual_keycode as usize] = *state == Pressed;
                    }
                    KeyboardInput {
                        input:
                            KeyInput {
                                virtual_keycode: None,
                                ..
                            },
                        ..
                    } => {}
                    ModifiersChanged(modifiers) => {
                        io.key_ctrl = modifiers.ctrl();
                        io.key_shift = modifiers.shift();
                        io.key_alt = modifiers.alt();
                        io.key_super = modifiers.logo();
                    }
                    CursorMoved { position, .. } => {
                        let scale = api.renderer.window.scale_factor() as f32;
                        io.mouse_pos = [position.x as f32 / scale, position.y as f32 / scale];
                    }
                    CursorEntered { .. } => {}
                    CursorLeft { .. } => {}
                    MouseWheel { .. } => {}
                    MouseInput { state, button, .. } => {
                        io.mouse_down[{
                            use crate::event::MouseButton::*;
                            match button {
                                Left => 0,
                                Right => 1,
                                Middle => 2,
                                Other(x) => *x as usize,
                            }
                        }] = *state == Pressed;
                    }
                    TouchpadPressure { .. } => {}
                    AxisMotion { .. } => {}
                    Touch(_) => {}
                    ScaleFactorChanged {
                        scale_factor,
                        new_inner_size,
                    } => {
                        io.display_size =
                            [new_inner_size.width as f32, new_inner_size.height as f32];
                        io.display_framebuffer_scale = [*scale_factor as f32, *scale_factor as f32];
                    }
                    ThemeChanged(_) => {}
                    Ime(_) => {}
                    Occluded(_) => {}
                    TouchpadMagnify {
                        device_id: _,
                        delta: _,
                        phase: _,
                    } => {}
                    SmartMagnify { device_id: _ } => {}
                    TouchpadRotate {
                        device_id: _,
                        delta: _,
                        phase: _,
                    } => {}
                }
            }
            DeviceEvent { event, .. } => {
                use crate::event::{
                    DeviceEvent::*, ElementState::*, KeyboardInput, MouseScrollDelta::*,
                };

                match event {
                    Added => {}
                    Removed => {}
                    MouseMotion { .. } => {}
                    MouseWheel {
                        delta: PixelDelta(PhysicalPosition { x, y }),
                    } => {
                        if self.has_focus {
                            io.mouse_wheel_h += (x / f64::from(5 * 24)) as f32;
                            io.mouse_wheel += (y / f64::from(5 * 24)) as f32;
                        }
                    }
                    Motion { .. } => {}
                    Button { state: Pressed, .. } => {}
                    Button {
                        button,
                        state: Released,
                    } => {
                        io.mouse_down[*button as usize] = false;
                    }
                    MouseWheel {
                        delta: LineDelta(x, y),
                    } => {
                        if self.has_focus {
                            io.mouse_wheel_h += x / 5.;
                            io.mouse_wheel += y / 5.;
                        }
                    }
                    Key(KeyboardInput {
                        state: Released,
                        virtual_keycode: Some(virtual_keycode),
                        ..
                    }) => {
                        io.keys_down[*virtual_keycode as usize] = false;
                    }
                    Key(_) => {}
                    Text { .. } => {}
                }
            }
            _ => {}
        }
        false
    }
}

pub struct InitLayer<UserEvent: 'static, UserData: UserDataTrait>(
    pub Option<Rc<RefCell<dyn Interface>>>,
    PhantomData<UserEvent>,
    PhantomData<UserData>,
    usize,
);

impl<UserEvent: 'static, UserData: UserDataTrait> InitLayer<UserEvent, UserData> {
    pub fn new(inner: Rc<RefCell<dyn Interface>>, layer_index: usize) -> Self {
        InitLayer(
            Some(inner),
            Default::default(),
            Default::default(),
            layer_index,
        )
    }
}

impl<UserEvent: 'static, UserData: 'static + UserDataTrait> crate::Layer<UserEvent, UserData>
    for InitLayer<UserEvent, UserData>
{
    fn get_name(&mut self) -> String {
        "Hazel Imgui Initial".to_owned()
    }

    fn on_preupdate(
        &mut self,
        api: &crate::AppAPI<UserEvent, UserData>,
        command_buffer: &CommandBuffer<VkHelperUser<UserData>>,
        _: &crate::EventLoopWindowTarget<UserEvent>,
        _: &mut crate::ControlFlow,
    ) {
        let mut layers = api.layers.borrow_mut();
        let index = layers[self.3]
            .iter()
            .position(|it| it.try_borrow_mut().is_err())
            .unwrap();

        layers[self.3].remove(index);

        let layer = Rc::new(RefCell::new(Layer::<UserEvent, UserData>::new(
            api,
            command_buffer,
            self.0.as_ref().unwrap().clone(),
        )));
        layers[self.3].insert(index, layer);
    }
}
