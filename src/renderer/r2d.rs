// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use indexmap::{indexmap, IndexMap};
use std::cell::{Cell, RefCell};
use std::collections::HashMap;
use std::fmt::{self, Debug};
use std::mem::ManuallyDrop;
use std::slice;

use derive_builder::Builder;
use memoffset::offset_of;

use super::pipeline::PipelineData;
use super::VertexBuffer;
use super::{UserDataTrait, VkHelperUser};
use crate::reexport::ash::vk;
use crate::reexport::glm::{self, Mat4, Vec2, Vec4};
use crate::reexport::vk_helper::{self as vkh, Error as VkHelperError};
use crate::reexport::vk_mem;
use crate::{glsl_fs, glsl_gs, glsl_vs, GLSLEmbedImpl};

// pub const IMAGE_ARRAY_SIZE: u32 = 4096;
// pub const IMAGE_WHITE: u32 = IMAGE_ARRAY_SIZE - 1;
pub const VERTEX_STRIDE: vk::DeviceSize = std::mem::size_of::<Vertex>() as _;
pub const VERTEX_STRIDE_U32: u32 = VERTEX_STRIDE as _;
pub const VERTEX_STRIDE_USIZE: usize = VERTEX_STRIDE as _;
pub const VERTEX_MIN_ALLOCATION: usize = 4096 / (VERTEX_STRIDE_USIZE - 4096 % VERTEX_STRIDE_USIZE);

use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    // #[snafu(display("vk_helper::wait_and_reset() error: {}", source))]
    // VkHelperWaitAndReset { source: VkHelperError },
    // #[snafu(display("file open error {}: {}", input.to_string_lossy(), source))]
    // Open {
    //    source: std::io::Error,
    //    input: std::path::PathBuf,
    // },
    #[snafu(display("hazel::Pipeline creation error: {}", source))]
    HazelBuildPipeline { source: super::pipeline::Error },
    #[snafu(display("vk_helper::Buffer vertex creation error: {}", source))]
    VkHelperVertexBufferNew { source: VkHelperError },
    #[snafu(display("vk_helper::DescriptorSet write buffer error: {}", source))]
    VkHelperDescriptorSetWriteBuffer { source: VkHelperError },
    #[snafu(display("vk_helper::DescriptorSet write sampler error: {}", source))]
    VkHelperDescriptorSetWriteSampler { source: VkHelperError },
    #[snafu(display("vk_helper::Buffer image creation error: {}", source))]
    VkHelperImageBufferNew { source: VkHelperError },
    #[snafu(display("failed get_allocation_info(): {}", source))]
    VkMemGetAllocationInfo { source: vk::Result },
    #[snafu(display("descriptor_set building error: {}", source))]
    VkHelperDescriptorSetNew { source: VkHelperError },
    #[snafu(display("vk_helper::Buffer uniform creation error: {}", source))]
    VkHelperUniformBufferNew { source: VkHelperError },
    #[snafu(display("buffer flushing error: {}", source))]
    VkHelperFlush { source: VkHelperError },
    #[snafu(display("vk_helper::DescriptorSet write image error: {}", source))]
    VkHelperDescriptorSetWriteImages { source: VkHelperError },
    #[snafu(display("hazel::BuildPipelineCreateInfo error: {}", source))]
    HazelBuildPipelineCreateInfo { source: super::pipeline::Error },
    #[snafu(display("vk_helper::Image creation error: {}", source))]
    VkHelperImageNew { source: VkHelperError },
    #[snafu(display("vk_helper::ImageView creation error: {}", source))]
    VkHelperImageViewNew { source: VkHelperError },
    #[snafu(display("derive_builder::Builder::build() error"))]
    DeriveBuilder {},
}

pub type Result<T> = std::result::Result<T, Error>;

pub use super::image::Image;

#[cfg(feature = "blue_is_alpha")]
pub enum Component<T: UserDataTrait> {
    Hex,
    Position(Vec2),
    Z(f32),
    Size(Vec2),
    Color(Vec4),
    Image(Image<T>, Option<((u32, u32), (u32, u32))>),
    BlueIsAlpha,
}

#[cfg(not(feature = "blue_is_alpha"))]
pub enum Component<T: UserDataTrait> {
    Hex,
    Position(Vec2),
    Z(f32),
    Size(Vec2),
    Color(Vec4),
    Image(Image<T>, Option<((u32, u32), (u32, u32))>),
}

pub type Entity<T> = Vec<Component<T>>;

#[repr(C)]
#[derive(Clone, Copy, Builder, Debug)]
#[cfg(feature = "blue_is_alpha")]
pub struct Vertex {
    pub transform: Mat4,
    pub color: Vec4,
    pub tc_range: Vec2,
    pub tc_min: Vec2,
    pub mtype: u32,
    pub image_id: u32,
    pub bits32: u32,
    pub _pad: u32,
}

#[repr(C)]
#[derive(Clone, Copy, Builder, Debug)]
#[cfg(not(feature = "blue_is_alpha"))]
pub struct Vertex {
    pub transform: Mat4,
    pub color: Vec4,
    pub tc_range: Vec2,
    pub tc_min: Vec2,
    pub mtype: u32,
    pub image_id: u32,
    pub _pad: Vec2,
}

impl Default for Vertex {
    #[cfg(feature = "blue_is_alpha")]
    fn default() -> Self {
        Self {
            transform: glm::identity(),
            color: glm::vec4(0., 0., 0., 0.),
            tc_range: glm::vec2(1., 1.),
            tc_min: glm::vec2(0., 0.),
            mtype: 0,
            image_id: 0,
            bits32: 0,
            _pad: 0,
        }
    }

    #[cfg(not(feature = "blue_is_alpha"))]
    fn default() -> Self {
        Self {
            transform: glm::identity(),
            color: glm::vec4(0., 0., 0., 0.),
            tc_range: glm::vec2(1., 1.),
            tc_min: glm::vec2(0., 0.),
            mtype: 0,
            image_id: 0,
            _pad: glm::vec2(0., 0.),
        }
    }
}

pub struct VertexList<T: UserDataTrait> {
    buffer: VertexBuffer<T>,
    slice: ManuallyDrop<Box<[Vertex]>>,
    size: usize,
    number: usize,
}

impl<T: UserDataTrait> Debug for VertexList<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "VertexList {{ size: {}, number: {} }}",
            self.size, self.number
        )
    }
}

pub struct VertexUserData<T: UserDataTrait>(vkh::Allocator<VkHelperUser<T>>);

impl<T: UserDataTrait> Debug for VertexUserData<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Allocator;")
    }
}

impl<T: UserDataTrait> goro::TokenList for VertexList<T> {
    type Error = Error;
    type UserData = VertexUserData<T>;

    fn create(size: usize, allocator: &mut Self::UserData) -> Result<Self> {
        let size =
            ((size + VERTEX_MIN_ALLOCATION - 1) / VERTEX_MIN_ALLOCATION) * VERTEX_MIN_ALLOCATION;
        let buffer = VertexBuffer {
            inner: vkh::Buffer::new(
                allocator.0.clone(),
                &vk::BufferCreateInfo::builder()
                    .size(VERTEX_STRIDE * size as vk::DeviceSize)
                    .usage(vk::BufferUsageFlags::VERTEX_BUFFER),
                &vk_mem::AllocationCreateInfo {
                    usage: vk_mem::MemoryUsage::Auto,
                    flags: vk_mem::AllocationCreateFlags::MAPPED
                        | vk_mem::AllocationCreateFlags::HOST_ACCESS_SEQUENTIAL_WRITE,
                    ..Default::default()
                },
                Default::default(),
            )
            .context(VkHelperVertexBufferNewSnafu {})?,
            stride: VERTEX_STRIDE_U32,
        };

        let allocation_info = unsafe {
            RefCell::borrow_mut(&buffer.allocator).get_allocation_info(&buffer.allocation)
        }
        .context(VkMemGetAllocationInfoSnafu {})?;
        let mapped_data = allocation_info.mapped_data.cast::<Vertex>();
        let slice = ManuallyDrop::new(unsafe {
            use std::slice::from_raw_parts_mut;
            Box::from_raw(from_raw_parts_mut(mapped_data, size))
        });

        Ok(Self {
            buffer,
            slice,
            size,
            number: 0,
        })
    }

    fn set_number(&mut self, number: usize) {
        self.number = number;
    }

    fn get_size(&self) -> usize {
        self.size
    }
}

pub struct Renderer<T: UserDataTrait> {
    pipeline: PipelineData<T>,
    vertex: RefCell<goro::Vector<VertexList<T>>>,
    pub images: RefCell<IndexMap<vkh::ImageView<VkHelperUser<T>>, i32>>,
    pub images_dirty: Cell<bool>,
    descriptor_set: vkh::DescriptorSet<VkHelperUser<T>>,
    #[cfg(feature)]
    pub command_buffer: vkh::CommandBuffer<VkHelperUser<T>>,
    pub stats_images_begin: Cell<usize>,
    pub stats_images: Cell<usize>,
    pub stats_vertex: RefCell<Vec<usize>>,
}

#[cfg(feature = "blue_is_alpha")]
static SHADERS: [&[u8]; 3] = [
    glsl_vs! { r#"#version 450 core

layout (location = 0) in mat4 inTransform;
layout (location = 4) in vec4 inColor;
layout (location = 5) in vec4 inTC;
layout (location = 6) in uint inType;
layout (location = 7) in uint inImageId;
layout (location = 8) in uint inBits32;

layout (location = 0) out mat4 outTransform;
layout (location = 4) out vec4 outColor;
layout (location = 5) out vec4 outTC;
layout (location = 6) out uint outType;
layout (location = 7) out uint outImageId;
layout (location = 8) out uint outBits32;

void main() {
    outTransform = inTransform;
    outColor = inColor;
    outTC = inTC;
    outType = inType;
    outImageId = inImageId;
    outBits32 = inBits32;
}"# },
    glsl_gs! { r#"#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices = 6) out;

layout (location = 0) in mat4 inTransform[];
layout (location = 4) in vec4 inColor[];
layout (location = 5) in vec4 inTC[];
layout (location = 6) in uint inType[];
layout (location = 7) in uint inImageId[];
layout (location = 8) in uint inBits32[];

layout (set = 0, binding = 0) uniform u_ViewProjection {
    mat4 ViewProjection;
} u0;

layout (location = 0) out vec4 outColor;
layout (location = 1) out vec2 outTexCoord;
layout (location = 2) out uint outImageId;
layout (location = 3) out uint outBits32;

float y = 0.433012694120407f;
vec2 coords[] = {
    vec2(-.5,.5),vec2(.5,.5),vec2(-.5,-.5),vec2(.5,-.5),
    vec2(-.5,0),vec2(-.25,y),vec2(-.25,-y),vec2(.25,y),vec2(.25,-y),vec2(.5,0),
};

vec2 tcs[] = {
    vec2(0,0),vec2(1,0),vec2(0,1),vec2(1,1),
    vec2(0,.5),vec2(.25,-y+.5),vec2(.25,y+.5),vec2(.75,-y+.5),vec2(.75,y+.5),vec2(1,.5),
};

uint begins[] = {0, 0, 4};
uint ends[]   = {9, 3, 9};

void main() {
    for (uint i = begins[inType[0]]; i <= ends[inType[0]]; i++) {
        outColor = inColor[0];
        outTexCoord = tcs[i] * inTC[0].xy + inTC[0].zw;
        outImageId = inImageId[0];
        outBits32 = inBits32[0];
        gl_Position = u0.ViewProjection * inTransform[0] * vec4(coords[i],0.,1.);
        EmitVertex();
    }
    EndPrimitive();
}"# },
    glsl_fs! { r#"#version 450 core

layout (location = 0) flat in vec4 inColor;
layout (location = 1) in vec2 inTexCoord;
layout (location = 2) flat in uint inImageId;
layout (location = 3) flat in uint inBits32;

layout (set = 0, binding = 1) uniform sampler inSampler;
layout (set = 0, binding = 2) uniform texture2D inTextures[30];

layout (location = 0) out vec4 outColor;

void main() {
    vec4 tmpColor = texture(sampler2D(inTextures[inImageId], inSampler), inTexCoord);
    if (0 != (inBits32 & 1) && tmpColor == vec4(0., 0., 1., 1.)) {
        outColor = vec4(inColor.xyz, 0.);
    } else {
        outColor = tmpColor * inColor;
    }
}"# },
];

#[cfg(not(feature = "blue_is_alpha"))]
static SHADERS: [&[u8]; 3] = [
    glsl_vs! { r#"#version 450 core

layout (location = 0) in mat4 inTransform;
layout (location = 4) in vec4 inColor;
layout (location = 5) in vec4 inTC;
layout (location = 6) in uint inType;
layout (location = 7) in uint inImageId;

layout (location = 0) out mat4 outTransform;
layout (location = 4) out vec4 outColor;
layout (location = 5) out vec4 outTC;
layout (location = 6) out uint outType;
layout (location = 7) out uint outImageId;

void main() {
    outTransform = inTransform;
    outColor = inColor;
    outTC = inTC;
    outType = inType;
    outImageId = inImageId;
}"# },
    glsl_gs! { r#"#version 450 core

layout (points) in;
layout (triangle_strip, max_vertices = 6) out;

layout (location = 0) in mat4 inTransform[];
layout (location = 4) in vec4 inColor[];
layout (location = 5) in vec4 inTC[];
layout (location = 6) in uint inType[];
layout (location = 7) in uint inImageId[];

layout (set = 0, binding = 0) uniform u_ViewProjection {
    mat4 ViewProjection;
} u0;

layout (location = 0) out vec4 outColor;
layout (location = 1) out vec2 outTexCoord;
layout (location = 2) out uint outImageId;

float y = 0.433012694120407f;
vec2 coords[] = {
    vec2(-.5,.5),vec2(.5,.5),vec2(-.5,-.5),vec2(.5,-.5),
    vec2(-.5,0),vec2(-.25,y),vec2(-.25,-y),vec2(.25,y),vec2(.25,-y),vec2(.5,0),
};

vec2 tcs[] = {
    vec2(0,0),vec2(1,0),vec2(0,1),vec2(1,1),
    vec2(0,.5),vec2(.25,-y+.5),vec2(.25,y+.5),vec2(.75,-y+.5),vec2(.75,y+.5),vec2(1,.5),
};

uint begins[] = {0, 0, 4};
uint ends[]   = {9, 3, 9};

void main() {
    for (uint i = begins[inType[0]]; i <= ends[inType[0]]; i++) {
        outColor = inColor[0];
        outTexCoord = tcs[i] * inTC[0].xy + inTC[0].zw;
        outImageId = inImageId[0];
        gl_Position = u0.ViewProjection * inTransform[0] * vec4(coords[i],0.,1.);
        EmitVertex();
    }
EndPrimitive();
}"# },
    glsl_fs! { r#"#version 450 core

layout (location = 0) flat in vec4 inColor;
layout (location = 1) in vec2 inTexCoord;
layout (location = 2) flat in uint inImageId;

layout (set = 0, binding = 1) uniform sampler inSampler;
layout (set = 0, binding = 2) uniform texture2D inTextures[30];

layout (location = 0) out vec4 outColor;

void main() {
    outColor = texture(sampler2D(inTextures[inImageId], inSampler), inTexCoord) * inColor;
}"# },
];

#[cfg(feature = "blue_is_alpha")]
fn add_bits32(attributes_hash: &mut HashMap<String, Vec<vk::VertexInputAttributeDescription>>) {
    attributes_hash.insert(
        "inBits32".to_owned(),
        vec![*vk::VertexInputAttributeDescription::builder()
            .format(vk::Format::R32_UINT)
            .offset(offset_of!(Vertex, bits32) as _)],
    );
}

#[cfg(not(feature = "blue_is_alpha"))]
fn add_bits32(_: &mut HashMap<String, Vec<vk::VertexInputAttributeDescription>>) {}

impl<T: UserDataTrait> Renderer<T> {
    pub fn get_device(&self) -> &vkh::Device<VkHelperUser<T>> {
        &self.descriptor_set.descriptor_pool.device
    }

    pub fn new(
        viewport_state_info: &vk::PipelineViewportStateCreateInfoBuilder,
        render_pass: vkh::RenderPass<VkHelperUser<T>>,
        allocator: vkh::Allocator<VkHelperUser<T>>,
        descriptor_pool: vkh::DescriptorPool<VkHelperUser<T>>,
        command_buffer: &vkh::CommandBuffer<VkHelperUser<T>>,
        sampler: vkh::Sampler<VkHelperUser<T>>,
    ) -> Result<Self> {
        use super::pipeline::*;
        use super::ShaderStageFlags;
        use ShaderStageInput::SpirV;

        let mut vertex = goro::Vector::<VertexList<T>>::new(
            VERTEX_MIN_ALLOCATION * 4,
            7,
            VertexUserData(allocator.clone()),
        )?;

        let mut shaders = HashMap::new();
        shaders.insert(ShaderStageFlags::VERTEX, SpirV(SHADERS[0].to_vec()));
        shaders.insert(ShaderStageFlags::GEOMETRY, SpirV(SHADERS[1].to_vec()));
        shaders.insert(ShaderStageFlags::FRAGMENT, SpirV(SHADERS[2].to_vec()));

        let mut attributes_hash = HashMap::new();
        #[allow(clippy::unneeded_field_pattern)]
        {
            attributes_hash.insert(
                "inTransform".to_owned(),
                vec![
                    *vk::VertexInputAttributeDescription::builder()
                        .format(vk::Format::R32G32B32A32_SFLOAT)
                        .offset(offset_of!(Vertex, transform) as u32),
                    *vk::VertexInputAttributeDescription::builder()
                        .format(vk::Format::R32G32B32A32_SFLOAT)
                        .offset(offset_of!(Vertex, transform) as u32 + 16),
                    *vk::VertexInputAttributeDescription::builder()
                        .format(vk::Format::R32G32B32A32_SFLOAT)
                        .offset(offset_of!(Vertex, transform) as u32 + 32),
                    *vk::VertexInputAttributeDescription::builder()
                        .format(vk::Format::R32G32B32A32_SFLOAT)
                        .offset(offset_of!(Vertex, transform) as u32 + 48),
                ],
            );
            attributes_hash.insert(
                "inColor".to_owned(),
                vec![*vk::VertexInputAttributeDescription::builder()
                    .format(vk::Format::R32G32B32A32_SFLOAT)
                    .offset(offset_of!(Vertex, color) as _)],
            );
            attributes_hash.insert(
                "inTC".to_owned(),
                vec![*vk::VertexInputAttributeDescription::builder()
                    .format(vk::Format::R32G32B32A32_SFLOAT)
                    .offset(offset_of!(Vertex, tc_range) as _)],
            );
            attributes_hash.insert(
                "inType".to_owned(),
                vec![*vk::VertexInputAttributeDescription::builder()
                    .format(vk::Format::R32_UINT)
                    .offset(offset_of!(Vertex, mtype) as _)],
            );
            attributes_hash.insert(
                "inImageId".to_owned(),
                vec![*vk::VertexInputAttributeDescription::builder()
                    .format(vk::Format::R32_UINT)
                    .offset(offset_of!(Vertex, image_id) as _)],
            );
            add_bits32(&mut attributes_hash);
        }

        let pipeline = build_pipeline(
            descriptor_pool.device.clone(),
            viewport_state_info,
            render_pass,
            slice::from_ref(&vertex.get_vector()[0].buffer.clone()),
            &attributes_hash,
            &ShaderStagesInput::Parts(Default::default(), shaders),
            vk::PrimitiveTopology::POINT_LIST,
            Default::default(),
        )
        .context(HazelBuildPipelineSnafu {})?
        .1;

        let tmp_pipeline = pipeline.pipeline.borrow().clone();
        let descriptor_set_layouts = {
            if let VkHelperUser::Pipeline {
                descriptor_set_layouts,
                ..
            } = &tmp_pipeline.user
            {
                descriptor_set_layouts
            } else {
                unreachable!()
            }
        };

        let descriptor_sets = vkh::DescriptorSet::new(
            &descriptor_pool,
            slice::from_ref(&descriptor_set_layouts.0[0]),
            vk::DescriptorSetAllocateInfo::builder(),
            Default::default(),
            vec![],
        )
        .context(VkHelperDescriptorSetNewSnafu {})?;
        let descriptor_set = descriptor_sets[0].clone();

        let uniform_buffer = vkh::Buffer::new(
            allocator.clone(),
            &vk::BufferCreateInfo::builder()
                .size(64)
                .usage(vk::BufferUsageFlags::UNIFORM_BUFFER),
            &vk_mem::AllocationCreateInfo {
                usage: vk_mem::MemoryUsage::Auto,
                flags: vk_mem::AllocationCreateFlags::MAPPED
                    | vk_mem::AllocationCreateFlags::HOST_ACCESS_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            Default::default(),
        )
        .context(VkHelperUniformBufferNewSnafu {})?;

        descriptor_set
            .write_buffer(
                uniform_buffer,
                vk::DescriptorBufferInfo::builder().offset(0).range(64),
                vk::WriteDescriptorSet::builder().dst_binding(0),
            )
            .context(VkHelperDescriptorSetWriteBufferSnafu {})?;

        descriptor_set
            .write_sampler(
                sampler,
                vk::DescriptorImageInfo::builder(),
                vk::WriteDescriptorSet::builder().dst_binding(1),
            )
            .context(VkHelperDescriptorSetWriteSamplerSnafu {})?;

        let white_image = {
            use crate::reexport::vk_mem::{AllocationCreateFlags, AllocationCreateInfo};

            let (format, image_width, image_height) = (vk::Format::R8G8B8A8_UNORM, 1, 1);

            let create_info = vk::BufferCreateInfo::builder()
                .size(4)
                .usage(vk::BufferUsageFlags::TRANSFER_SRC);

            let image_buffer = vkh::Buffer::new(
                allocator.clone(),
                &create_info,
                &AllocationCreateInfo {
                    usage: vk_mem::MemoryUsage::Auto,
                    flags: AllocationCreateFlags::MAPPED
                        | vk_mem::AllocationCreateFlags::HOST_ACCESS_SEQUENTIAL_WRITE,
                    ..Default::default()
                },
                Default::default(),
            )
            .context(VkHelperImageBufferNewSnafu {})?;

            let create_info = vk::ImageCreateInfo::builder()
                .image_type(vk::ImageType::TYPE_2D)
                .format(format)
                .extent(vk::Extent3D {
                    width: image_width,
                    height: image_height,
                    depth: 1,
                })
                .mip_levels(1)
                .array_layers(1)
                .samples(vk::SampleCountFlags::TYPE_1)
                .usage(vk::ImageUsageFlags::SAMPLED | vk::ImageUsageFlags::TRANSFER_DST)
                .initial_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL);

            let image = crate::reexport::vk_helper::Image::new(
                allocator,
                &create_info,
                &AllocationCreateInfo {
                    usage: vk_mem::MemoryUsage::Auto,
                    ..Default::default()
                },
                Default::default(),
            )
            .context(VkHelperImageNewSnafu {})?;

            use std::os::raw::c_uchar;
            let image_base = unsafe {
                RefCell::borrow_mut(&image_buffer.allocator)
                    .get_allocation_info(&image_buffer.allocation)
            }
            .unwrap()
            .mapped_data as *mut c_uchar;
            unsafe {
                image_base.copy_from_nonoverlapping([0xffu8, 0xff, 0xff, 0xff].as_ptr(), 4);
            };

            let buffer_image_copy = vk::BufferImageCopy::builder()
                .image_subresource(
                    *vk::ImageSubresourceLayers::builder()
                        .aspect_mask(vk::ImageAspectFlags::COLOR)
                        .layer_count(1),
                )
                .image_extent(vk::Extent3D {
                    width: image_width,
                    height: image_height,
                    depth: 1,
                });
            command_buffer.copy_buffer_to_image(
                image_buffer,
                image.clone(),
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                slice::from_ref(&*buffer_image_copy),
            );

            let shader_from_transfer = vk::ImageMemoryBarrier::builder()
                .src_access_mask(vk::AccessFlags::TRANSFER_WRITE)
                .dst_access_mask(vk::AccessFlags::SHADER_READ)
                .old_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
                .new_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                .subresource_range(
                    *vk::ImageSubresourceRange::builder()
                        .aspect_mask(vk::ImageAspectFlags::COLOR)
                        .level_count(1)
                        .layer_count(1),
                );

            command_buffer.pipeline_barrier(
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::FRAGMENT_SHADER,
                vk::DependencyFlags::empty(),
                &[],
                vec![],
                vec![(image.clone(), shader_from_transfer)],
            );

            let create_info = vk::ImageViewCreateInfo::builder()
                .view_type(vk::ImageViewType::TYPE_2D)
                .format(format)
                .subresource_range(
                    *vk::ImageSubresourceRange::builder()
                        .aspect_mask(vk::ImageAspectFlags::COLOR)
                        .level_count(1)
                        .layer_count(1),
                );

            vkh::ImageView::new(image, create_info, Default::default())
                .context(VkHelperImageViewNewSnafu {})?
        };

        Ok(Self {
            pipeline,
            vertex: vertex.into(),
            descriptor_set,
            images: indexmap! { white_image => 0 /* ignored */ }.into(),
            images_dirty: true.into(),
            stats_images_begin: 0.into(),
            stats_images: 1.into(),
            stats_vertex: vec![].into(),
        })
    }

    pub(crate) fn resize(&self, renderer: &super::Renderer<T>) -> Result<()> {
        let viewport_ref = renderer.viewport.borrow();
        let viewport = viewport_ref.as_ref().unwrap();
        let viwports = viewport.viewports;
        let scissors = viewport.scissors.clone();
        let viewport_state_info = vk::PipelineViewportStateCreateInfo::builder()
            .viewports(slice::from_ref(&viwports))
            .scissors(&scissors);

        let pipeline_layout = self.pipeline.pipeline.borrow().layout.clone();
        let user = self.pipeline.pipeline.borrow().user.clone();
        self.pipeline.pipeline.replace(
            super::pipeline::build_pipeline_create_info(
                &viewport_state_info,
                viewport.get_render_pass().clone(),
                &[],
                &[],
                pipeline_layout,
                self.pipeline.shaders.clone(),
                vk::PrimitiveTopology::POINT_LIST,
                user,
            )
            .context(HazelBuildPipelineCreateInfoSnafu {})?,
        );

        Ok(())
    }

    pub fn begin_scene(&self, view_projection: &Mat4) -> Result<()> {
        if let Some(vkh::DescriptorSetContents::UniformBuffer(uniform_buffer)) =
            &self.descriptor_set.contents.borrow()[0]
        {
            use crate::reexport::ash::util::Align;
            use std::mem::align_of;
            use std::ptr::NonNull;
            self.vertex.borrow_mut().tick()?;
            let allocation_info = unsafe {
                RefCell::borrow_mut(&uniform_buffer.allocator)
                    .get_allocation_info(&uniform_buffer.allocation)
            }
            .context(VkMemGetAllocationInfoSnafu {})?;
            let mapped_data = allocation_info.mapped_data;

            let mut uniform_align = unsafe {
                Align::new(
                    NonNull::new_unchecked(mapped_data).cast().as_mut(),
                    align_of::<f32>() as u64,
                    64,
                )
            };
            uniform_align.copy_from_slice(glm::value_ptr(view_projection));
            uniform_buffer.flush(0, 0).context(VkHelperFlushSnafu {})?;
        } else {
            unreachable!()
        };

        // There could be unused images?
        let mut images = RefCell::borrow_mut(&self.images);
        let mut images_iter = images.iter_mut();
        // Skip white_image.
        images_iter.next();
        let empties: Vec<_> = images_iter
            .rev() // Useless optimise, for removing from end.
            .map(|(image, count)| {
                *count -= 1;
                (image, *count)
            })
            .filter(|(_, new_count)| *new_count == 0)
            .map(|(k, _)| k.clone())
            .collect();
        if !empties.is_empty() {
            self.images_dirty.set(true);
        }
        for empty in empties {
            images.remove(&empty);
        }
        self.stats_images_begin.set(images.len());
        *self.stats_vertex.borrow_mut() = vec![];
        Ok(())
    }
}

impl<T: UserDataTrait> VertexList<T> {
    fn draw(
        &self,
        command_buffer: &vkh::CommandBuffer<VkHelperUser<T>>,
        stat: &mut Vec<usize>,
    ) -> Result<()> {
        if self.number == 0 {
            return Ok(());
        };
        self.buffer
            .flush(0, self.number * VERTEX_STRIDE_USIZE)
            .context(VkHelperFlushSnafu {})?;
        command_buffer.bind_vertex_buffers(0, vec![self.buffer.inner.clone()], slice::from_ref(&0));
        unsafe {
            command_buffer.command_pool.device.cmd_draw(
                ***command_buffer,
                self.number as u32,
                1,
                0,
                0,
            );
        }
        stat.push(self.number);

        Ok(())
    }
}

impl<T: UserDataTrait> Renderer<T> {
    pub fn end_scene(
        &self,
        command_buffer: &vkh::CommandBuffer<super::VkHelperUser<T>>,
    ) -> Result<()> {
        if self.images_dirty.get() {
            self.stats_images.set(self.images.borrow().len());
            self.descriptor_set
                .write_images(
                    self.images
                        .borrow_mut()
                        .iter()
                        .map(|(image, _)| {
                            (
                                image.clone(),
                                vk::DescriptorImageInfo::builder()
                                    .image_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                            )
                        })
                        .collect::<Vec<_>>(),
                    vk::WriteDescriptorSet::builder().dst_binding(2),
                )
                .context(VkHelperDescriptorSetWriteImagesSnafu {})?;
        }
        self.images_dirty.set(false); // Must be here, at start is true.
        let pipeline: &PipelineData<T> = &self.pipeline.clone();
        command_buffer.bind_pipeline(pipeline.pipeline.borrow().clone());
        command_buffer
            .bind_descriptor_sets(0, slice::from_ref(&self.descriptor_set), &[])
            .context(VkHelperFlushSnafu {})?;
        let mut stats = self.stats_vertex.borrow_mut();
        self.vertex
            .borrow_mut()
            .get_vector()
            .into_iter()
            .try_for_each(|e| e.draw(command_buffer, &mut stats))
    }

    #[inline]
    fn handle_image_component<'a>(
        &self,
        i: Image<T>,
        sub_image: Option<((u32, u32), (u32, u32))>,
        acc: &'a mut VertexBuilder,
    ) -> &'a mut VertexBuilder {
        let mut images = self.images.borrow_mut();
        let e = images.entry(i.0);
        let index = e.index() as u32;
        e.and_modify(|count| *count = 10).or_insert_with(|| {
            self.images_dirty.set(true);
            10
        });
        if let Some(((min_x, min_y), (size_x, size_y))) = sub_image {
            acc.tc_range(glm::vec2(
                size_x as f32 / i.1.width as f32,
                size_y as f32 / i.1.height as f32,
            ))
            .tc_min(glm::vec2(
                min_x as f32 / i.1.width as f32,
                min_y as f32 / i.1.height as f32,
            ))
        } else {
            acc
        }
        .image_id(index)
    }

    #[cfg(feature = "blue_is_alpha")]
    #[inline]
    pub fn draw(&mut self, entity: Entity<T>) -> Result<()> {
        use ieee754::Ieee754;
        use Component::*;
        static mut Z_CTR: f32 = 1f32;
        unsafe {
            Z_CTR = Z_CTR.prev();
        }
        let mut bits32 = 0u32;
        let mut transform = Mat4::identity();
        unsafe {
            transform.row_mut(2)[3] = Z_CTR;
        }
        let vertex = &entity
            .into_iter()
            .fold(
                VertexBuilder::default()
                    .mtype(1)
                    .color(glm::vec4(1f32, 1., 1., 1.))
                    .tc_range(glm::vec2(1., 1.))
                    .tc_min(glm::vec2(0., 0.))
                    .image_id(0)
                    .bits32(0)
                    ._pad(0),
                |acc, x| match x {
                    Hex => acc.mtype(2),
                    Position(v) => {
                        transform.row_mut(0)[3] = v[0];
                        transform.row_mut(1)[3] = v[1];
                        acc
                    }
                    Z(z) => {
                        transform.row_mut(2)[3] = if z == 1. { 1f32.prev() } else { z };
                        acc
                    }
                    Size(s) => {
                        transform.row_mut(0)[0] = s[0];
                        transform.row_mut(1)[1] = s[1];
                        acc
                    }
                    Color(c) => acc.color(c),
                    Image(i, sub_image) => self.handle_image_component(i, sub_image, acc),
                    BlueIsAlpha => {
                        bits32 |= 1;
                        acc.bits32(bits32)
                    }
                },
            )
            .transform(transform)
            .build()
            .map_err(|e| {
                dbg!(e);
                snafu::NoneError
            })
            .context(DeriveBuilderSnafu {})?;
        self.push_vertex(vertex)
    }

    #[cfg(not(feature = "blue_is_alpha"))]
    #[inline]
    pub fn draw(&self, entity: Entity<T>) -> Result<()> {
        use ieee754::Ieee754;
        use Component::*;
        static mut Z_CTR: f32 = 1f32;
        unsafe {
            Z_CTR = Z_CTR.prev();
        }
        let mut transform = Mat4::identity();
        unsafe {
            transform.row_mut(2)[3] = Z_CTR;
        }
        let vertex = &entity
            .into_iter()
            .fold(
                VertexBuilder::default()
                    .mtype(1)
                    .color(glm::vec4(1f32, 1., 1., 1.))
                    .tc_range(glm::vec2(1., 1.))
                    .tc_min(glm::vec2(0., 0.))
                    .image_id(0)
                    ._pad(glm::vec2(0., 0.)),
                |acc, x| match x {
                    Hex => acc.mtype(2),
                    Position(v) => {
                        transform.row_mut(0)[3] = v[0];
                        transform.row_mut(1)[3] = v[1];
                        acc
                    }
                    Z(z) => {
                        transform.row_mut(2)[3] = if (z - 1.).abs() < f32::EPSILON {
                            1f32.prev()
                        } else {
                            z
                        };
                        acc
                    }
                    Size(s) => {
                        transform.row_mut(0)[0] = s[0];
                        transform.row_mut(1)[1] = s[1];
                        acc
                    }
                    Color(c) => acc.color(c),
                    Image(i, sub_image) => self.handle_image_component(i, sub_image, acc),
                },
            )
            .transform(transform)
            .build()
            .map_err(|e| {
                dbg!(e);
                snafu::NoneError
            })
            .context(DeriveBuilderSnafu {})?;
        self.push_vertex(vertex)
    }

    pub fn push_vertex(&self, data: &Vertex) -> Result<()> {
        let mut vertex = self.vertex.borrow_mut();
        let (ref mut token, ptr) = vertex.get_token()?;
        token.slice[ptr] = *data;

        Ok(())
    }
}
