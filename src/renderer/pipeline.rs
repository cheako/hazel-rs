// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::cell::RefCell;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::ffi::CString;
use std::fmt::Debug;
use std::path::PathBuf;

use super::{UserDataTrait, VertexBuffer, VkHelperUser};
use crate::reexport::ash::vk;
use crate::reexport::vk_helper::Error as VkHelperError;
use crate::reexport::vk_helper::*;

use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("pipeline creation error: {}", source))]
    VkHelperPipelineNew { source: VkHelperError },
    #[snafu(display("file open error {}: {}", input.to_string_lossy(), source))]
    Open {
        source: std::io::Error,
        input: std::path::PathBuf,
    },
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug)]
pub enum ShaderVariableType {
    Push {
        offset: usize,
        range: usize,
    },
    UniformBufferMember {
        set: u32,
        binding: u32,
        offset: u32,
        size: u32,
    },
    UniformSampler2D {
        set: u32,
        binding: u32,
    },
    UniformSampler {
        set: u32,
        binding: u32,
    },
    UniformTexture2DArray {
        set: u32,
        binding: u32,
    },
}

#[derive(Clone)]
pub struct DescriptorSetLayouts<T: UserDataTrait>(pub Box<[DescriptorSetLayout<VkHelperUser<T>>]>);

impl<T: UserDataTrait> Debug for DescriptorSetLayouts<T> {
    fn fmt(&self, _fmt: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        /* TODO: write something */
        Ok(())
    }
}

#[derive(Clone, Debug)]
pub enum ShaderStageInput {
    Code(String),
    SpirV(Vec<u8>),
}

#[derive(Clone)]
pub enum ShaderStagesInput {
    Parts(String, HashMap<vk::ShaderStageFlags, ShaderStageInput>),
    File(PathBuf),
}

type ShaderHashMap<T> = HashMap<vk::ShaderStageFlags, (CString, ShaderModule<VkHelperUser<T>>)>;

#[allow(clippy::too_many_arguments)]
pub(crate) fn build_pipeline_create_info<T: UserDataTrait>(
    viewport_state_info: &vk::PipelineViewportStateCreateInfoBuilder,
    render_pass: RenderPass<VkHelperUser<T>>,
    vertexs: &[VertexBuffer<T>],
    attributes: &[vk::VertexInputAttributeDescription],
    pipeline_layout: PipelineLayout<VkHelperUser<T>>,
    shaders: ShaderHashMap<T>,
    topology: vk::PrimitiveTopology,
    user: VkHelperUser<T>,
) -> Result<Pipeline<VkHelperUser<T>>> {
    let vertex_input_binding_descriptions: Vec<_> = vertexs
        .iter()
        .map(|it| {
            *vk::VertexInputBindingDescription::builder()
                .stride(it.stride)
                .input_rate(vk::VertexInputRate::VERTEX)
        })
        .collect();

    let vertex_input_state_info = vk::PipelineVertexInputStateCreateInfo::builder()
        .vertex_attribute_descriptions(attributes)
        .vertex_binding_descriptions(&vertex_input_binding_descriptions);

    let iter = shaders.iter();
    let shader_stage_create_infos: Vec<_> = iter
        .clone()
        .map(|(k, v)| {
            vk::PipelineShaderStageCreateInfo::builder()
                .name(&v.0)
                .stage(*k)
        })
        .collect();

    let shaders: HashMap<_, _> = iter.map(|(&k, v)| (k, v.1.clone())).collect();

    let vertex_input_assembly_state_info =
        vk::PipelineInputAssemblyStateCreateInfo::builder().topology(topology);
    let rasterization_info = vk::PipelineRasterizationStateCreateInfo::builder()
        .polygon_mode(vk::PolygonMode::FILL)
        .line_width(1.)
        .cull_mode(vk::CullModeFlags::BACK)
        .front_face(vk::FrontFace::CLOCKWISE)
        .depth_bias_enable(false);
    let multisample_state_info = vk::PipelineMultisampleStateCreateInfo::builder()
        .rasterization_samples(vk::SampleCountFlags::TYPE_1)
        .min_sample_shading(1.);
    let noop_stencil_state = vk::StencilOpState::builder().compare_op(vk::CompareOp::ALWAYS);
    let depth_state_info = vk::PipelineDepthStencilStateCreateInfo::builder()
        .depth_test_enable(true)
        .depth_write_enable(true)
        .depth_compare_op(vk::CompareOp::LESS)
        .front(*noop_stencil_state)
        .back(*noop_stencil_state)
        .max_depth_bounds(1.0);
    let color_blend_attachment_state = vk::PipelineColorBlendAttachmentState::builder()
        .color_write_mask(vk::ColorComponentFlags::RGBA)
        .blend_enable(true)
        .src_color_blend_factor(vk::BlendFactor::SRC_ALPHA)
        .dst_color_blend_factor(vk::BlendFactor::ONE_MINUS_SRC_ALPHA)
        .color_blend_op(vk::BlendOp::ADD)
        .src_alpha_blend_factor(vk::BlendFactor::ONE)
        .dst_alpha_blend_factor(vk::BlendFactor::ZERO)
        .alpha_blend_op(vk::BlendOp::ADD);
    let color_blend_state = vk::PipelineColorBlendStateCreateInfo::builder()
        .attachments(std::slice::from_ref(&color_blend_attachment_state));

    let create_info = vk::GraphicsPipelineCreateInfo::builder()
        .vertex_input_state(&vertex_input_state_info)
        .input_assembly_state(&vertex_input_assembly_state_info)
        .viewport_state(viewport_state_info)
        .rasterization_state(&rasterization_info)
        .multisample_state(&multisample_state_info)
        .depth_stencil_state(&depth_state_info)
        .color_blend_state(&color_blend_state);
    Pipeline::new(
        pipeline_layout,
        render_pass,
        shaders,
        shader_stage_create_infos,
        create_info,
        user,
    )
    .context(VkHelperPipelineNewSnafu)
}

fn find_subsequence(haystack: &[u8], needle: &str) -> Option<usize> {
    haystack
        .windows(needle.len())
        .position(|window| window == needle.as_bytes())
}

#[derive(Clone, Debug)]
pub enum DescriptorSetBinding {
    Buffer {
        stage_flags: vk::ShaderStageFlags,
        size: u32,
    },
    Sampler2D {
        stage_flags: vk::ShaderStageFlags,
    },
    Sampler {
        stage_flags: vk::ShaderStageFlags,
    },
    Texture2DArray {
        stage_flags: vk::ShaderStageFlags,
    },
}

#[derive(Clone)]
pub struct PipelineData<T: UserDataTrait> {
    pub pipeline: RefCell<Pipeline<VkHelperUser<T>>>,
    pub shaders: ShaderHashMap<T>,
    pub attributes: Vec<vk::VertexInputAttributeDescription>,
    pub topology: vk::PrimitiveTopology,
}

#[allow(clippy::too_many_arguments)]
pub fn build_pipeline<T: UserDataTrait, S: ::std::hash::BuildHasher>(
    device: Device<VkHelperUser<T>>,
    viewport_state_info: &vk::PipelineViewportStateCreateInfoBuilder,
    render_pass: RenderPass<VkHelperUser<T>>,
    vertexes: &[VertexBuffer<T>],
    attributes_hash: &HashMap<String, Vec<vk::VertexInputAttributeDescription>, S>,
    input: &ShaderStagesInput,
    topology: vk::PrimitiveTopology,
    user: T,
) -> Result<(String, PipelineData<T>)> {
    use ShaderStagesInput::*;
    match input {
        File(input) => {
            use std::io::Read;
            let mut parts: HashMap<vk::ShaderStageFlags, ShaderStageInput> = HashMap::new();

            let mut file = std::fs::File::open(&input).context(OpenSnafu { input })?;

            let mut data = Vec::new();
            file.read_to_end(&mut data)
                .unwrap_or_else(|why| panic!("couldn't slurp {}: {:?}", input.display(), why));

            const TYPE_TOKEN: &str = "#type";
            let mut pos = find_subsequence(&data, TYPE_TOKEN);
            while let Some(_pos) = pos {
                use std::str::from_utf8;
                data = data[_pos + TYPE_TOKEN.len() + 1..].to_vec();
                let iter = data.iter();
                let t = iter
                    .clone()
                    .scan(Vec::new(), |t, &val| match val {
                    10 | 13 /* '\n' | '\r' */
                    => None, v => { t.push(v); Some(t.clone()) } })
                    .last()
                    .unwrap();
                data = iter
                    .skip(t.len())
                    .skip_while(|val| matches!(val, 10 | 13))
                    .copied()
                    .collect();
                pos = find_subsequence(&data, TYPE_TOKEN);
                parts.insert(
                    match from_utf8(&t[..]).unwrap() {
                        "vertex" => vk::ShaderStageFlags::VERTEX,
                        "fragment" | "pixel" => vk::ShaderStageFlags::FRAGMENT,
                        x => panic!("Unknown shader type: {}", x),
                    },
                    ShaderStageInput::Code(
                        from_utf8(if let Some(_pos) = pos {
                            &data[.._pos]
                        } else {
                            &data[..]
                        })
                        .unwrap()
                        .to_owned(),
                    ),
                );
            }

            build_pipeline(
                device,
                viewport_state_info,
                render_pass,
                vertexes,
                attributes_hash,
                &Parts(
                    input.file_stem().unwrap().to_str().unwrap().to_owned(),
                    parts,
                ),
                topology,
                user,
            )
        }
        Parts(name, input) => {
            use ::ash_tray::ash::vk;
            let mut attributes: Vec<vk::VertexInputAttributeDescription> = Default::default();
            let mut type_data_by_name = HashMap::new();
            let mut descriptor_set_layout: BTreeMap<_, HashMap<u32, DescriptorSetBinding>> =
                BTreeMap::new();
            let mut push_constant_ranges: HashMap<_, (String, vk::ShaderStageFlags, usize)> =
                Default::default();
            let mut push_size = 0usize;
            let mut uniform_buffer_size = 0u32;
            let shaders: HashMap<_, _> = input
                .iter()
                .map(|(stage_flags, v)| {
                    use ShaderStageInput::*;

                    let spirv = match v {
                        SpirV(v) => v.clone(),
                        Code(v) => {
                            use shaderc::{Compiler, ShaderKind};
                            Compiler::new()
                                .unwrap()
                                .compile_into_spirv(
                                    v,
                                    match *stage_flags {
                                        vk::ShaderStageFlags::VERTEX => ShaderKind::Vertex,
                                        vk::ShaderStageFlags::FRAGMENT => ShaderKind::Fragment,
                                        _ => unimplemented!(),
                                    },
                                    "on_the_spot_code.glsl",
                                    "main",
                                    None,
                                )
                                .unwrap()
                                .as_binary_u8()
                                .to_vec()
                        }
                    };

                    use spirv_cross::{glsl, spirv};
                    let words = spirv
                        .chunks(4)
                        .map(|array| {
                            (u32::from(array[3]) << 24)
                                + (u32::from(array[2]) << 16)
                                + (u32::from(array[1]) << 8)
                                + u32::from(array[0])
                        })
                        .collect::<Vec<_>>();
                    let module = spirv::Module::from_words(&words[..]);
                    let ast = spirv::Ast::<glsl::Target>::parse(&module).unwrap();

                    let shader_resources = ast.get_shader_resources().unwrap();

                    /* Calc pipeline_layout */
                    if *stage_flags == vk::ShaderStageFlags::VERTEX {
                        shader_resources
                            .stage_inputs
                            .iter()
                            /* Todo: None should not be ignored! */
                            .for_each(|resource| {
                                let binding = ast
                                    .get_decoration(resource.id, spirv::Decoration::Binding)
                                    .unwrap();
                                let base_location = ast
                                    .get_decoration(resource.id, spirv::Decoration::Location)
                                    .unwrap();
                                attributes_hash
                                    .get(&resource.name)
                                    .unwrap()
                                    .iter()
                                    .enumerate()
                                    .for_each(|(id, attribute)| {
                                        let mut attribute = *attribute;
                                        attribute.binding = binding;
                                        attribute.location = base_location + id as u32;
                                        attributes.push(attribute)
                                    });
                            });
                    }

                    for resource in shader_resources.push_constant_buffers.iter() {
                        let ranges = ast.get_active_buffer_ranges(resource.id).unwrap();
                        for it in ranges.into_iter() {
                            let spirv::BufferRange {
                                index,
                                offset,
                                range,
                            } = it;
                            let name = ast
                                .get_member_name(resource.base_type_id, index as _)
                                .unwrap();
                            push_constant_ranges
                                .entry(offset)
                                .and_modify(|v| {
                                    assert_eq!(v.0, *name);
                                    v.1 |= *stage_flags;
                                    assert_eq!(v.2, range);
                                })
                                .or_insert_with(|| {
                                    type_data_by_name.insert(
                                        name.clone(),
                                        ShaderVariableType::Push { offset, range },
                                    );
                                    (name.clone(), *stage_flags, range)
                                });
                            let end = offset + range;
                            if end > push_size {
                                push_size = end;
                            };
                        }
                    }

                    for resource in shader_resources.uniform_buffers.iter() {
                        let descriptor_set_id = ast
                            .get_decoration(resource.id, spirv::Decoration::DescriptorSet)
                            .unwrap();
                        let binding_id = ast
                            .get_decoration(resource.id, spirv::Decoration::Binding)
                            .unwrap();
                        let reasource_type = ast.get_type(resource.base_type_id).unwrap();
                        if let spirv::Type::Struct { member_types, .. } = reasource_type {
                            for index in 0..member_types.len() {
                                type_data_by_name.insert(
                                    ast.get_member_name(resource.base_type_id, index as _)
                                        .unwrap(),
                                    ShaderVariableType::UniformBufferMember {
                                        set: descriptor_set_id,
                                        binding: binding_id,
                                        offset: ast
                                            .get_member_decoration(
                                                resource.base_type_id,
                                                index as _,
                                                spirv::Decoration::Offset,
                                            )
                                            .unwrap(),
                                        size: ast
                                            .get_declared_struct_member_size(
                                                resource.base_type_id,
                                                index as _,
                                            )
                                            .unwrap(),
                                    },
                                );
                            }
                        }
                        let size = ast.get_declared_struct_size(resource.base_type_id).unwrap();
                        descriptor_set_layout
                            .entry(descriptor_set_id)
                            .and_modify(|e| {
                                e.insert(
                                    binding_id,
                                    DescriptorSetBinding::Buffer {
                                        stage_flags: *stage_flags,
                                        size,
                                    },
                                );
                            })
                            .or_insert_with(|| {
                                let mut e = HashMap::new();
                                e.insert(
                                    binding_id,
                                    DescriptorSetBinding::Buffer {
                                        stage_flags: *stage_flags,
                                        size,
                                    },
                                );
                                e
                            });
                    }

                    for resource in shader_resources.sampled_images.iter() {
                        let descriptor_set_id = ast
                            .get_decoration(resource.id, spirv::Decoration::DescriptorSet)
                            .unwrap();
                        let binding_id = ast
                            .get_decoration(resource.id, spirv::Decoration::Binding)
                            .unwrap();
                        type_data_by_name.insert(
                            resource.name.clone(),
                            ShaderVariableType::UniformSampler2D {
                                set: descriptor_set_id,
                                binding: binding_id,
                            },
                        );
                        descriptor_set_layout
                            .entry(descriptor_set_id)
                            .and_modify(|e| {
                                e.insert(
                                    binding_id,
                                    DescriptorSetBinding::Sampler2D {
                                        stage_flags: *stage_flags,
                                    },
                                );
                            })
                            .or_insert_with(|| {
                                let mut e = HashMap::new();
                                e.insert(
                                    binding_id,
                                    DescriptorSetBinding::Sampler2D {
                                        stage_flags: *stage_flags,
                                    },
                                );
                                e
                            });
                    }

                    for resource in shader_resources.separate_images.iter() {
                        let descriptor_set_id = ast
                            .get_decoration(resource.id, spirv::Decoration::DescriptorSet)
                            .unwrap();
                        let binding_id = ast
                            .get_decoration(resource.id, spirv::Decoration::Binding)
                            .unwrap();
                        let is_array = true; // ast.get_type(resource.type_id).unwrap().image_arrayed.
                        type_data_by_name.insert(
                            resource.name.clone(),
                            if is_array {
                                ShaderVariableType::UniformTexture2DArray {
                                    set: descriptor_set_id,
                                    binding: binding_id,
                                }
                            } else {
                                unimplemented!()
                            },
                        );
                        descriptor_set_layout
                            .entry(descriptor_set_id)
                            .and_modify(|e| {
                                /* TODO: Remove duplicate. */
                                let binding = if is_array {
                                    DescriptorSetBinding::Texture2DArray {
                                        stage_flags: *stage_flags,
                                    }
                                } else {
                                    unimplemented!()
                                };
                                e.insert(binding_id, binding);
                            })
                            .or_insert_with(|| {
                                let binding = if is_array {
                                    DescriptorSetBinding::Texture2DArray {
                                        stage_flags: *stage_flags,
                                    }
                                } else {
                                    unimplemented!()
                                };
                                let mut e = HashMap::new();
                                e.insert(binding_id, binding);
                                e
                            });
                    }

                    for resource in shader_resources.separate_samplers.iter() {
                        let descriptor_set_id = ast
                            .get_decoration(resource.id, spirv::Decoration::DescriptorSet)
                            .unwrap();
                        let binding_id = ast
                            .get_decoration(resource.id, spirv::Decoration::Binding)
                            .unwrap();
                        type_data_by_name.insert(
                            resource.name.clone(),
                            ShaderVariableType::UniformSampler {
                                set: descriptor_set_id,
                                binding: binding_id,
                            },
                        );
                        descriptor_set_layout
                            .entry(descriptor_set_id)
                            .and_modify(|e| {
                                e.insert(
                                    binding_id,
                                    DescriptorSetBinding::Sampler {
                                        stage_flags: *stage_flags,
                                    },
                                );
                            })
                            .or_insert_with(|| {
                                let mut e = HashMap::new();
                                e.insert(
                                    binding_id,
                                    DescriptorSetBinding::Sampler {
                                        stage_flags: *stage_flags,
                                    },
                                );
                                e
                            });
                    }

                    (
                        *stage_flags,
                        (
                            CString::new(
                                ast.get_entry_points().unwrap_or_else(|_| {
                                    vec![spirv::EntryPoint {
                                        name: "main".to_owned(),
                                        execution_model: spirv::ExecutionModel::Vertex,
                                        work_group_size: spirv::WorkGroupSize { x: 0, y: 0, z: 0 },
                                    }]
                                })[0]
                                    .name
                                    .clone(),
                            )
                            .unwrap_or_else(|e| {
                                dbg!(e);
                                CString::new("main".to_owned()).unwrap()
                            }),
                            ShaderModule::new_simple(device.clone(), &spirv, Default::default())
                                .unwrap(),
                        ),
                    )
                })
                .collect();

            let mut this_set = 0u32;
            #[allow(clippy::type_complexity)]
            let (descriptor_set_layouts, uniform_buffer_layouts): (
                Vec<_>,
                Vec<Box<_>>,
            ) = descriptor_set_layout
                .into_iter()
                .flat_map(|(set, bindings)| {
                    let mut ret = Vec::new();
                    while this_set < set {
                        ret.push((
                            DescriptorSetLayout::new(
                                device.clone(),
                                [].into(),
                                vk::DescriptorSetLayoutCreateInfo::builder(),
                                Default::default(),
                            )
                            .unwrap(),
                            [].into(),
                        ));
                        this_set += 1;
                    }

                    let (bindings, offsets): (Vec<_>, Vec<_>) = bindings
                        .into_iter()
                        .map(|(id, binding)| {
                            let (binding, size) = match binding {
                                DescriptorSetBinding::Buffer { stage_flags, size } => (
                                    *vk::DescriptorSetLayoutBinding::builder()
                                        .binding(id)
                                        .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
                                        .descriptor_count(1)
                                        .stage_flags(stage_flags),
                                    size,
                                ),
                                DescriptorSetBinding::Sampler2D { stage_flags } => (
                                    *ash_tray::ash::vk::DescriptorSetLayoutBinding::builder()
                                        .binding(id)
                                        .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                                        .descriptor_count(1)
                                        .stage_flags(stage_flags),
                                    0,
                                ),
                                DescriptorSetBinding::Sampler { stage_flags } => (
                                    *ash_tray::ash::vk::DescriptorSetLayoutBinding::builder()
                                        .binding(id)
                                        .descriptor_type(vk::DescriptorType::SAMPLER)
                                        .descriptor_count(1)
                                        .stage_flags(stage_flags),
                                    0,
                                ),
                                DescriptorSetBinding::Texture2DArray { stage_flags } => (
                                    *ash_tray::ash::vk::DescriptorSetLayoutBinding::builder()
                                        .binding(id)
                                        .descriptor_type(vk::DescriptorType::SAMPLED_IMAGE)
                                        /* TODO: This is kinda random. */
                                        .descriptor_count(4096)
                                        .stage_flags(stage_flags),
                                    0,
                                ),
                            };
                            let ret = (
                                binding,
                                if size == 0 {
                                    None
                                } else {
                                    Some((uniform_buffer_size, size))
                                },
                            );
                            uniform_buffer_size += size;
                            ret
                        })
                        .unzip();
                    ret.push((
                        DescriptorSetLayout::new(
                            device.clone(),
                            bindings.into_boxed_slice(),
                            vk::DescriptorSetLayoutCreateInfo::builder(),
                            Default::default(),
                        )
                        .unwrap(),
                        offsets.into_boxed_slice(),
                    ));
                    this_set += 1;
                    ret
                })
                .unzip();

            let vk_push_constant_ranges: Vec<_> = push_constant_ranges
                .iter()
                .map(|(offset, (_, stage_flags, size))| {
                    *vk::PushConstantRange::builder()
                        .stage_flags(*stage_flags)
                        .offset(*offset as _)
                        .size(*size as _)
                })
                .collect();
            let create_info = vk::PipelineLayoutCreateInfo::builder()
                .push_constant_ranges(&vk_push_constant_ranges[..]);

            let pipeline_layout = PipelineLayout::new(
                device,
                descriptor_set_layouts.clone().into_boxed_slice(),
                create_info,
                Default::default(),
            )
            .unwrap();

            let descriptor_set_layouts =
                DescriptorSetLayouts(descriptor_set_layouts.into_boxed_slice());

            let pipeline = RefCell::new(build_pipeline_create_info(
                viewport_state_info,
                render_pass,
                vertexes,
                &attributes,
                pipeline_layout,
                shaders.clone(),
                topology,
                VkHelperUser::Pipeline {
                    user,
                    push_size,
                    push_constant_ranges,
                    uniform_buffer_size,
                    uniform_buffer_layouts: uniform_buffer_layouts.into_boxed_slice(),
                    type_data_by_name,
                    descriptor_set_layouts,
                },
            )?);

            Ok((
                name.clone(),
                PipelineData {
                    pipeline,
                    shaders,
                    attributes,
                    topology,
                },
            ))
        }
    }
}
