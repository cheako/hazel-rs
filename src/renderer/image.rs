// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::fmt::Debug;
use std::path::{Path, PathBuf};

use crate::reexport::ash::vk;
use crate::reexport::vk_helper::{self, Error as VkHelperError};
use crate::reexport::vk_mem;
use crate::reexport::vk_mem::{AllocationCreateFlags, AllocationCreateInfo};

pub use super::base::Renderer;
use super::PipelineVariableValue;

use ash_tray::vk_helper::CommandBuffer;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("image open error {}: {}", input.to_string_lossy(), source))]
    ImageOpen {
        source: image::ImageError,
        input: PathBuf,
    },
    #[snafu(display("vk_helper::Buffer creation error: {}", source))]
    VkHelperBufferNew { source: VkHelperError },
    #[snafu(display("vk_helper::Image creation error: {}", source))]
    VkHelperImageNew { source: VkHelperError },
    #[snafu(display("vk_helper::ImageView creation error: {}", source))]
    VkHelperImageViewNew { source: VkHelperError },
    #[snafu(display("safe_transmute::transmute_to_bytes_vec error: {}", source))]
    Transmute {
        source: safe_transmute::Error<'static, u16, u8>,
    },
}

pub type Result<T> = std::result::Result<T, Error>;

pub type Image<T> = (vk_helper::ImageView<super::VkHelperUser<T>>, vk::Extent3D);

impl<T: super::UserDataTrait> Renderer<T> {
    /// Issues render commands, can only be called from on_preupdate()
    pub fn load_image<P: AsRef<Path> + Clone>(
        &self,
        command_buffer: &CommandBuffer<super::VkHelperUser<T>>,
        input: P,
    ) -> Result<Image<T>> {
        use crate::reexport::vk_helper::*;
        use image::DynamicImage::*;

        let ret = image::open(input.clone()).context(ImageOpenSnafu {
            input: input.as_ref().to_path_buf(),
        })?;

        use safe_transmute::transmute_to_bytes_vec as t;
        let (format, (image_width, image_height), image_pixels) = match ret {
            // Each pixel in this image is 8-bit Luma
            ImageLuma8(i) => (vk::Format::R8_UNORM, i.dimensions(), i.into_raw()),

            // Each pixel in this image is 8-bit Luma with alpha
            ImageLumaA8(i) => (vk::Format::R8G8_UNORM, i.dimensions(), i.into_raw()),

            // Each pixel in this image is 8-bit Rgb
            ImageRgb8(i) => (vk::Format::R8G8B8A8_UNORM, i.dimensions(), {
                let mut vec = i.into_raw();
                vec.push(255u8);
                for i in (1..vec.len() - 2).rev() {
                    if i % 3 == 0 {
                        vec.insert(i, 255u8);
                    }
                }
                vec
            }),

            // Each pixel in this image is 8-bit Rgb with alpha
            ImageRgba8(i) => (vk::Format::R8G8B8A8_UNORM, i.dimensions(), i.into_raw()),

            // Each pixel in this image is 16-bit Luma
            ImageLuma16(i) => (
                vk::Format::R16_UNORM,
                i.dimensions(),
                t(i.into_raw()).context(TransmuteSnafu {})?,
            ),

            // Each pixel in this image is 16-bit Luma with alpha
            ImageLumaA16(i) => (
                vk::Format::R16G16_UNORM,
                i.dimensions(),
                t(i.into_raw()).context(TransmuteSnafu {})?,
            ),

            // Each pixel in this image is 16-bit Rgb
            ImageRgb16(i) => (
                vk::Format::R16G16B16A16_UNORM,
                i.dimensions(),
                t(i.into_raw()).context(TransmuteSnafu {})?,
            ),

            // Each pixel in this image is 16-bit Rgb with alpha
            ImageRgba16(i) => (
                vk::Format::R16G16B16A16_UNORM,
                i.dimensions(),
                t(i.into_raw()).context(TransmuteSnafu {})?,
            ),
            ImageRgb32F(_) => todo!(),
            ImageRgba32F(_) => todo!(),
            _ => todo!(),
        };

        let create_info = vk::BufferCreateInfo::builder()
            .size(image_pixels.len() as _)
            .usage(vk::BufferUsageFlags::TRANSFER_SRC);

        let image_buffer = Buffer::new(
            self.allocator.clone(),
            &create_info,
            &AllocationCreateInfo {
                usage: vk_mem::MemoryUsage::Auto,
                flags: AllocationCreateFlags::MAPPED
                    | AllocationCreateFlags::HOST_ACCESS_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            Default::default(),
        )
        .context(VkHelperBufferNewSnafu {})?;

        let extent = vk::Extent3D {
            width: image_width,
            height: image_height,
            depth: 1,
        };

        let create_info = vk::ImageCreateInfo::builder()
            .image_type(vk::ImageType::TYPE_2D)
            .format(format)
            .extent(extent)
            .mip_levels(1)
            .array_layers(1)
            .samples(vk::SampleCountFlags::TYPE_1)
            .usage(vk::ImageUsageFlags::SAMPLED | vk::ImageUsageFlags::TRANSFER_DST)
            .initial_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL);

        let image = Image::new(
            self.allocator.clone(),
            &create_info,
            &AllocationCreateInfo {
                usage: vk_mem::MemoryUsage::Auto,
                ..Default::default()
            },
            Default::default(),
        )
        .context(VkHelperImageNewSnafu {})?;

        let create_info = vk::ImageViewCreateInfo::builder()
            .view_type(vk::ImageViewType::TYPE_2D)
            .format(format)
            .subresource_range(
                *vk::ImageSubresourceRange::builder()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .level_count(1)
                    .layer_count(1),
            );

        let image_view = ImageView::new(image.clone(), create_info, Default::default())
            .context(VkHelperImageViewNewSnafu {})?;

        use std::os::raw::c_uchar;
        let image_base = unsafe {
            image_buffer
                .allocator
                .borrow_mut()
                .get_allocation_info(&image_buffer.allocation)
        }
        .unwrap()
        .mapped_data as *mut c_uchar;
        unsafe {
            image_base.copy_from_nonoverlapping(image_pixels.as_ptr(), image_pixels.len());
        };

        let buffer_image_copy = vk::BufferImageCopy::builder()
            .image_subresource(
                *vk::ImageSubresourceLayers::builder()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .layer_count(1),
            )
            .image_extent(extent);
        command_buffer.copy_buffer_to_image(
            image_buffer,
            image.clone(),
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            std::slice::from_ref(&*buffer_image_copy),
        );

        let shader_from_transfer = vk::ImageMemoryBarrier::builder()
            .src_access_mask(vk::AccessFlags::TRANSFER_WRITE)
            .dst_access_mask(vk::AccessFlags::SHADER_READ)
            .old_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
            .new_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .subresource_range(
                *vk::ImageSubresourceRange::builder()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .level_count(1)
                    .layer_count(1),
            );
        command_buffer.pipeline_barrier(
            vk::PipelineStageFlags::TRANSFER,
            vk::PipelineStageFlags::FRAGMENT_SHADER,
            vk::DependencyFlags::empty(),
            &[],
            vec![],
            vec![(image, shader_from_transfer)],
        );

        Ok((image_view, extent))
    }

    /// Issues render commands, can only be called from on_preupdate()
    pub fn load_combined_image_sampler<P: AsRef<Path> + Clone>(
        &self,
        command_buffer: &CommandBuffer<super::VkHelperUser<T>>,
        input: P,
    ) -> Result<PipelineVariableValue<T>> {
        Ok(PipelineVariableValue::Sampler2D(
            self.sampler.clone(),
            self.load_image(command_buffer, input)?,
        ))
    }
}
