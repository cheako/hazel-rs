use crate::reexport::ash::vk;
use crate::reexport::vk_helper::{Error as VkHelperError, Framebuffer, ImageView, RenderPass};

use super::base::UserDataTrait;
use super::VkHelperUser;

use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("vk_helper::wait_and_reset() error: {}", source))]
    VkHelperWaitAndReset { source: VkHelperError },
    #[snafu(display("vk_helper::RenderTarget::new() error: {}", source))]
    VkHelperRenderTargetNew { source: VkHelperError },
    #[snafu(display("file open error {}: {}", input.to_string_lossy(), source))]
    Open {
        source: std::io::Error,
        input: std::path::PathBuf,
    },
    #[snafu(display("hazel::Renderer2DNew() error: {}", source))]
    HazelRenderer2DNew { source: super::r2d::Error },
    #[snafu(display("hazel::SetViewProjection2D() error: {}", source))]
    HazelSetViewProjection2D { source: super::r2d::Error },
    #[snafu(display("hazel::Renderer2DResize() error: {}", source))]
    HazelRenderer2DResize { source: super::r2d::Error },
    #[snafu(display("hazel::VertexArrayResize() error: {}", source))]
    HazelVertexArrayResize { source: super::render::Error },
    #[snafu(display("sampler creation error: {}", source))]
    VkHelperSamplerNew { source: VkHelperError },
    #[snafu(display("hazel::EndScene2D() error: {}", source))]
    HazelEndScene2D { source: super::r2d::Error },
}
pub type Result<T> = std::result::Result<T, Error>;

#[derive(derive_builder::Builder)]
pub struct RenderTargetCreateInfo {
    pub width: u32,
    pub height: u32,
    #[cfg(feature = "unimplemented")]
    pub format: vk::Format,
    #[cfg(feature = "unimplemented")]
    pub samples: u32,
}

pub struct RenderTarget<T: UserDataTrait> {
    pub framebuffer: Framebuffer<VkHelperUser<T>>,
    pub create_info: RenderTargetCreateInfo,
}

impl<T: UserDataTrait> RenderTarget<T> {
    pub fn new(
        render_pass: RenderPass<VkHelperUser<T>>,
        attachments: Vec<ImageView<VkHelperUser<T>>>,
        spec: RenderTargetCreateInfo,
    ) -> Result<Self> {
        let create_info = vk::FramebufferCreateInfo::builder()
            .width(spec.width)
            .height(spec.height)
            .layers(1);

        Ok(Self {
            framebuffer: Framebuffer::new(
                render_pass,
                attachments,
                create_info,
                Default::default(),
            )
            .context(VkHelperRenderTargetNewSnafu {})?,
            create_info: spec,
        })
    }
}
