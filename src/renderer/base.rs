// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::cell::RefCell;
use std::collections::HashMap;
use std::ffi::CString;
use std::fmt::Debug;
use std::rc::Rc;

use super::r2d::Renderer as Renderer2D;
use super::VkHelperUser;
use crate::reexport::ash::vk;
use crate::reexport::ash_tray::vk_make_api_version;
use crate::reexport::glm::Mat4;
use crate::reexport::vk_helper::{Error as VkHelperError, *};
use crate::reexport::vk_mem;
use crate::reexport::winit::window::Window;

use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("vk_helper::wait_and_reset() error: {}", source))]
    VkHelperWaitAndReset { source: VkHelperError },
    #[snafu(display("file open error {}: {}", input.to_string_lossy(), source))]
    Open {
        source: std::io::Error,
        input: std::path::PathBuf,
    },
    #[snafu(display("hazel::Renderer2DNew() error: {}", source))]
    HazelRenderer2DNew { source: super::r2d::Error },
    #[snafu(display("hazel::SetViewProjection2D() error: {}", source))]
    HazelSetViewProjection2D { source: super::r2d::Error },
    #[snafu(display("hazel::Renderer2DResize() error: {}", source))]
    HazelRenderer2DResize { source: super::r2d::Error },
    #[snafu(display("hazel::VertexArrayResize() error: {}", source))]
    HazelVertexArrayResize { source: super::render::Error },
    #[snafu(display("sampler creation error: {}", source))]
    VkHelperSamplerNew { source: VkHelperError },
    #[snafu(display("hazel::EndScene2D() error: {}", source))]
    HazelEndScene2D { source: super::r2d::Error },
}

pub type Result<T> = std::result::Result<T, Error>;

pub trait UserDataTrait: Clone + Default + Debug {}
impl<TU: Clone + Default + Debug> UserDataTrait for TU {}

#[derive(Clone, Debug)]
pub enum PipelineVariableValue<T: UserDataTrait> {
    I8(i8),
    I16(i16),
    I32(i32),
    U8(u8),
    U16(u16),
    U32(u32),
    F32(f32),
    VecI8(Vec<i8>),
    VecI16(Vec<i16>),
    VecI32(Vec<i32>),
    VecU8(Vec<u8>),
    VecU16(Vec<u16>),
    VecU32(Vec<u32>),
    VecF32(Vec<f32>),
    Mat4(Mat4),
    Sampler2D(Sampler<VkHelperUser<T>>, super::image::Image<T>),
    Sampler(Sampler<VkHelperUser<T>>),
    Textures(Vec<ImageView<VkHelperUser<T>>>),
}

impl<T: UserDataTrait> PipelineVariableValue<T> {
    pub fn get_image(&self) -> &super::image::Image<T> {
        match self {
            Self::Sampler2D(_, i) => i,
            _ => unimplemented!(),
        }
    }
}

pub struct Renderer<T: UserDataTrait> {
    pub surface: SurfaceKHR<VkHelperUser<T>>,
    pub allocator: Allocator<VkHelperUser<T>>,
    pub window: Window,
    pub present_queue: Queue<VkHelperUser<T>>,
    pub present_complete_semaphore: BinarySemaphore<VkHelperUser<T>>,
    pub timeline_semaphore: TimelineSemaphore<VkHelperUser<T>>,
    pub frame_ctr: std::cell::Cell<u64>,
    pub viewport: RefCell<Option<RendererViewport<T>>>,
    pub vertex_arrays: RefCell<Vec<Rc<super::render::VertexArray<T>>>>,
    pub background_variables: HashMap<String, PipelineVariableValue<T>>,
    pub renderer_2d: Renderer2D<T>,
    pub descriptor_pool: DescriptorPool<VkHelperUser<T>>,
    pub sampler: Sampler<VkHelperUser<T>>,
}

pub struct NextImage<T: UserDataTrait> {
    pub inner: Framebuffer<VkHelperUser<T>>,
    pub command_buffer: CommandBuffer<VkHelperUser<T>>,
    pub rendering_complete: BinarySemaphore<VkHelperUser<T>>,
}

pub struct RendererViewport<T: UserDataTrait> {
    pub surface_resolution: vk::Extent2D,

    pub swapchain: SwapchainKHR<VkHelperUser<T>>,
    pub next_images: Vec<NextImage<T>>,

    pub viewports: vk::Viewport,
    pub scissors: Vec<vk::Rect2D>,
}

impl<T: UserDataTrait> Renderer<T> {
    pub(crate) fn new(
        window: Window,
        window_width: u32,
        window_height: u32,
        application_version: u32,
    ) -> Result<Self> {
        use crate::reexport::ash::extensions::khr::Swapchain;
        use ash_tray::WindowExt;

        let app_name = CString::new("Hazel").unwrap();
        let app_info = vk::ApplicationInfo::builder()
            .application_name(&app_name)
            .application_version(application_version)
            .engine_name(&app_name)
            .engine_version(vk_make_api_version(0, 0, 1, 0))
            .api_version(vk_make_api_version(0, 1, 2, 0));

        let extension_names_raw = window.get_required_extensions().unwrap();
        let create_info = vk::InstanceCreateInfo::builder()
            .application_info(&app_info)
            .enabled_extension_names(&extension_names_raw);

        let instance = Instance::new_simple(&create_info, VkHelperUser::<T>::default())
            .expect("Instance creation error");

        let surface = SurfaceKHR::new(instance.clone(), &window, 0, Default::default())
            .expect("Surface creation error");

        let device_extension_names_raw = [Swapchain::name().as_ptr()];
        let features = vk::PhysicalDeviceFeatures::builder()
            .shader_clip_distance(true)
            .geometry_shader(true);

        let create_info = [*vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(surface.queue_family_index)
            .queue_priorities(&[1.0])];

        let mut timeline_semaphores_features =
            vk::PhysicalDeviceTimelineSemaphoreFeatures::builder().timeline_semaphore(true);

        let create_info = vk::DeviceCreateInfo::builder()
            .queue_create_infos(&create_info)
            .enabled_extension_names(&device_extension_names_raw)
            .enabled_features(&features)
            .push_next(&mut timeline_semaphores_features);

        let device = Device::new(
            instance.clone(),
            surface.physical_device,
            &create_info,
            Default::default(),
        )
        .expect("Device creation error");
        let create_info = vk_mem::AllocatorCreateInfo::<
            RcInstance<VkHelperUser<T>>,
            RcDevice<VkHelperUser<T>>,
        >::new(instance.inner, device.0.clone(), surface.physical_device);
        let allocator = Allocator::new(
            surface.physical_device,
            device.clone(),
            create_info,
            Default::default(),
        )
        .expect("vk_mem: Allocator creation error");
        let present_queue = Queue::new(
            device.clone(),
            surface.queue_family_index,
            0,
            Default::default(),
        );

        let present_complete_semaphore =
            BinarySemaphore::new(device.clone(), &Default::default(), Default::default())
                .expect("Present Semaphore creation error");
        let timeline_semaphore = TimelineSemaphore::new_simple(
            device.clone(),
            vk::SemaphoreCreateInfo::builder(),
            Default::default(),
        )
        .expect("Timeline Semaphore creation error");

        let create_info = vk::CommandPoolCreateInfo::builder()
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .queue_family_index(surface.queue_family_index);

        let command_pool = CommandPool::new(device.clone(), &create_info, Default::default())
            .expect("CommandPool creation error");

        let create_info =
            vk::CommandBufferAllocateInfo::builder().level(vk::CommandBufferLevel::PRIMARY);

        let command_buffer = CommandBuffer::new(
            command_pool.clone(),
            CommandBufferFence::TimelineSemaphore(timeline_semaphore.clone(), Default::default()),
            create_info,
            Default::default(),
        )
        .expect("CommandBuffer creation error");

        let begin_info = vk::CommandBufferBeginInfo::builder()
            .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        unsafe { device.begin_command_buffer(**command_buffer, &begin_info) }
            .expect("Begin command buffer");

        let descriptor_pool_sizes = [
            *vk::DescriptorPoolSize::builder()
                .ty(vk::DescriptorType::UNIFORM_BUFFER)
                .descriptor_count(4096),
            *vk::DescriptorPoolSize::builder()
                .ty(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                .descriptor_count(4096),
            *vk::DescriptorPoolSize::builder()
                .ty(vk::DescriptorType::SAMPLED_IMAGE)
                .descriptor_count(16),
            *vk::DescriptorPoolSize::builder()
                .ty(vk::DescriptorType::SAMPLER)
                .descriptor_count(16),
        ];
        let create_info = vk::DescriptorPoolCreateInfo::builder()
            .flags(vk::DescriptorPoolCreateFlags::FREE_DESCRIPTOR_SET)
            .max_sets(8192)
            .pool_sizes(&descriptor_pool_sizes);

        let descriptor_pool = DescriptorPool::new(device.clone(), &create_info, Default::default())
            .expect("DescriptorPool creation error");

        let sampler = Sampler::new(
            device.clone(),
            &vk::SamplerCreateInfo::builder().min_filter(vk::Filter::LINEAR),
            Default::default(),
        )
        .context(VkHelperSamplerNewSnafu {})?;

        window.set_visible(true);
        let viewport = RendererViewport::new(
            &command_pool,
            &timeline_semaphore,
            surface.clone(),
            allocator.clone(),
            present_queue.clone(),
            window_width,
            window_height,
        );

        let renderer_2d = {
            let viwports = viewport.viewports;
            let scissors = viewport.scissors.clone();
            let viewport_state_info = vk::PipelineViewportStateCreateInfo::builder()
                .viewports(std::slice::from_ref(&viwports))
                .scissors(&scissors);
            Renderer2D::new(
                &viewport_state_info,
                viewport.get_render_pass().clone(),
                allocator.clone(),
                descriptor_pool.clone(),
                &command_buffer,
                sampler.clone(),
            )
            .context(HazelRenderer2DNewSnafu {})?
        };

        unsafe { device.end_command_buffer(**command_buffer) }.expect("End command buffer");

        command_buffer.set_signal_value(3);
        command_buffer
            .queue_submit(
                present_queue.clone(),
                vec![],
                vec![],
                vec![],
                vec![],
                vk::SubmitInfo::builder(),
            )
            .expect("queue submit failed.");
        command_buffer
            .wait_and_reset(std::u64::MAX)
            .context(VkHelperWaitAndResetSnafu)?;

        Ok(Self {
            surface,
            allocator,
            window,
            present_queue,
            present_complete_semaphore,
            timeline_semaphore,
            frame_ctr: Default::default(),
            viewport: RefCell::new(Some(viewport)),
            vertex_arrays: Default::default(),
            background_variables: Default::default(),
            renderer_2d,
            descriptor_pool,
            sampler,
        })
    }

    pub fn get_device(&self) -> &Device<VkHelperUser<T>> {
        &self.present_queue.device
    }

    pub fn begin_scene(&self, view_projection: &Mat4) -> Result<()> {
        self.renderer_2d
            .begin_scene(view_projection)
            .context(HazelSetViewProjection2DSnafu {})
    }

    pub fn end_scene(&self, command_buffer: &CommandBuffer<super::VkHelperUser<T>>) -> Result<()> {
        self.renderer_2d
            .end_scene(command_buffer)
            .context(HazelEndScene2DSnafu {})
    }
}

impl<T: UserDataTrait> RendererViewport<T> {
    pub(crate) fn new(
        command_pool: &CommandPool<VkHelperUser<T>>,
        timeline_semaphore: &TimelineSemaphore<VkHelperUser<T>>,
        surface: SurfaceKHR<VkHelperUser<T>>,
        allocator: Allocator<VkHelperUser<T>>,
        queue: Queue<VkHelperUser<T>>,
        window_width: u32,
        window_height: u32,
    ) -> Self {
        let device = allocator.device.clone();
        let surface_capabilities = unsafe {
            surface
                .loader
                .get_physical_device_surface_capabilities(surface.physical_device, **surface)
        }
        .unwrap();
        let mut desired_image_count = surface_capabilities.min_image_count + 1;
        if surface_capabilities.max_image_count > 0
            && desired_image_count > surface_capabilities.max_image_count
        {
            desired_image_count = surface_capabilities.max_image_count;
        }
        let surface_resolution = match surface_capabilities.current_extent.width {
            std::u32::MAX => vk::Extent2D {
                width: window_width,
                height: window_height,
            },
            _ => surface_capabilities.current_extent,
        };
        let pre_transform = if surface_capabilities
            .supported_transforms
            .contains(vk::SurfaceTransformFlagsKHR::IDENTITY)
        {
            vk::SurfaceTransformFlagsKHR::IDENTITY
        } else {
            surface_capabilities.current_transform
        };
        let present_modes = unsafe {
            surface
                .loader
                .get_physical_device_surface_present_modes(surface.physical_device, **surface)
        }
        .unwrap();

        let present_mode = present_modes
            .iter()
            .cloned()
            .find(|&mode| mode == vk::PresentModeKHR::MAILBOX)
            .unwrap_or(vk::PresentModeKHR::FIFO);

        let render_pass_attachments = [
            vk::AttachmentDescription {
                format: surface.formats.first().unwrap().format,
                samples: vk::SampleCountFlags::TYPE_1,
                load_op: vk::AttachmentLoadOp::CLEAR,
                final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
                ..Default::default()
            },
            vk::AttachmentDescription {
                format: vk::Format::D32_SFLOAT_S8_UINT,
                samples: vk::SampleCountFlags::TYPE_1,
                load_op: vk::AttachmentLoadOp::CLEAR,
                // store_op: vk::AttachmentStoreOp::DONT_CARE,
                initial_layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                final_layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                ..Default::default()
            },
        ];
        let color_attachment_refs = [vk::AttachmentReference {
            attachment: 0,
            layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
        }];
        let depth_attachment_ref = vk::AttachmentReference {
            attachment: 1,
            layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        };
        let dependencies = [vk::SubpassDependency {
            src_subpass: vk::SUBPASS_EXTERNAL,
            src_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            dst_access_mask: vk::AccessFlags::COLOR_ATTACHMENT_READ
                | vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
            dst_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            ..Default::default()
        }];

        let subpasses = [vk::SubpassDescription::builder()
            .color_attachments(&color_attachment_refs)
            .depth_stencil_attachment(&depth_attachment_ref)
            .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
            .build()];

        let create_info = vk::RenderPassCreateInfo::builder()
            .attachments(&render_pass_attachments)
            .subpasses(&subpasses)
            .dependencies(&dependencies);

        let render_pass = RenderPass::new(device.clone(), &create_info, Default::default())
            .expect("Render Pass creation error");

        let create_info = vk::SwapchainCreateInfoKHR::builder()
            .min_image_count(desired_image_count)
            .image_color_space(surface.formats.first().unwrap().color_space)
            .image_format(surface.formats.first().unwrap().format)
            .image_extent(surface_resolution)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .pre_transform(pre_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(present_mode)
            .clipped(true)
            .image_array_layers(1);

        let sync_objects: Box<_> = std::iter::repeat_with(|| {
            (
                BinarySemaphore::new(device.clone(), &Default::default(), Default::default())
                    .unwrap(),
                Fence::new(device.clone(), &Default::default(), Default::default()).unwrap(),
            )
        })
        .take(create_info.min_image_count as _)
        .collect();

        let swapchain = SwapchainKHR::new(
            device.clone(),
            surface.clone(),
            queue,
            create_info,
            sync_objects,
            Default::default(),
            Default::default(),
            vec![],
        )
        .expect("Swapchain creation error");

        let next_images: Vec<_> = swapchain
            .present_images
            .iter()
            .map(|image| {
                let create_info = vk::ImageViewCreateInfo::builder()
                    .view_type(vk::ImageViewType::TYPE_2D)
                    .format(surface.formats.first().unwrap().format)
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    });

                ImageView::new(image.clone(), create_info, Default::default())
                    .expect("Present Image View creation error")
            })
            .map(|present| {
                let create_info =
                    vk::CommandBufferAllocateInfo::builder().level(vk::CommandBufferLevel::PRIMARY);

                let command_buffer = CommandBuffer::new(
                    command_pool.clone(),
                    CommandBufferFence::TimelineSemaphore(
                        timeline_semaphore.clone(),
                        Default::default(),
                    ),
                    create_info,
                    Default::default(),
                )
                .expect("CommandBuffer creation error");

                let create_info = vk::ImageCreateInfo::builder()
                    .image_type(vk::ImageType::TYPE_2D)
                    .format(vk::Format::D32_SFLOAT_S8_UINT)
                    .extent(vk::Extent3D {
                        width: surface_resolution.width,
                        height: surface_resolution.height,
                        depth: 1,
                    })
                    .mip_levels(1)
                    .array_layers(1)
                    .samples(vk::SampleCountFlags::TYPE_1)
                    .tiling(vk::ImageTiling::OPTIMAL)
                    .usage(vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT)
                    .sharing_mode(vk::SharingMode::EXCLUSIVE)
                    .initial_layout(vk::ImageLayout::DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL);

                let depth = Image::new(
                    allocator.clone(),
                    &create_info,
                    &vk_mem::AllocationCreateInfo {
                        usage: vk_mem::MemoryUsage::Auto,
                        ..Default::default()
                    },
                    Default::default(),
                )
                .expect("Image creation error");

                let create_info = vk::ImageViewCreateInfo::builder()
                    .subresource_range(
                        vk::ImageSubresourceRange::builder()
                            .aspect_mask(
                                vk::ImageAspectFlags::DEPTH | vk::ImageAspectFlags::STENCIL,
                            )
                            .level_count(1)
                            .layer_count(1)
                            .build(),
                    )
                    .format(vk::Format::D32_SFLOAT_S8_UINT)
                    .view_type(vk::ImageViewType::TYPE_2D);

                let depth = ImageView::new(depth.clone(), create_info, Default::default())
                    .expect("Image View creation error");
                let create_info = vk::FramebufferCreateInfo::builder()
                    .width(surface_resolution.width)
                    .height(surface_resolution.height)
                    .layers(1);

                let inner = Framebuffer::new(
                    render_pass.clone(),
                    vec![present.clone(), depth.clone()],
                    create_info,
                    Default::default(),
                )
                .expect("Framebuffer creation error");

                let rendering_complete =
                    BinarySemaphore::new(device.clone(), &Default::default(), Default::default())
                        .expect("Rendering Semaphore creation error");

                NextImage {
                    inner,
                    rendering_complete,
                    command_buffer,
                }
            })
            .collect();

        Self {
            surface_resolution,
            swapchain,
            next_images,
            viewports: vk::Viewport {
                x: 0.0,
                y: 0.0,
                width: surface_resolution.width as f32,
                height: surface_resolution.height as f32,
                min_depth: 0.0,
                max_depth: 1.0,
            },
            scissors: vec![vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: surface_resolution,
            }],
        }
    }

    pub fn get_render_pass(&self) -> &RenderPass<VkHelperUser<T>> {
        &self.next_images[0].inner.render_pass
    }

    pub(crate) fn dissolve(
        self,
    ) -> (
        CommandPool<VkHelperUser<T>>,
        TimelineSemaphore<VkHelperUser<T>>,
    ) {
        (
            self.next_images[0].command_buffer.command_pool.clone(),
            if let CommandBufferFence::TimelineSemaphore(x, _) =
                self.next_images[0].command_buffer.0.fence.clone()
            {
                x
            } else {
                unreachable!()
            },
        )
    }
}

impl<T: UserDataTrait> Renderer<T> {
    pub(crate) fn resize_pipelines(&self) -> Result<()> {
        self.renderer_2d
            .resize(self)
            .context(HazelRenderer2DResizeSnafu {})?;
        for v in self.vertex_arrays.borrow().iter() {
            v.resize(self).context(HazelVertexArrayResizeSnafu {})?
        }
        Ok(())
    }
}
