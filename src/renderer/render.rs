// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//  http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::collections::HashMap;
use std::fmt::Debug;
use std::mem::size_of_val;
use std::rc::Rc;
use std::slice;

use super::{pipeline::*, *};
use crate::reexport::ash::vk;
use crate::reexport::vk_helper::{Error as VkHelperError, *};
use crate::reexport::vk_mem;

use ::image::EncodableLayout;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("pipeline creation error: {}", source))]
    VkHelperPipelineNew { source: VkHelperError },
    #[snafu(display("pipeline building error: {}", source))]
    BuildPipeline { source: VkHelperError },
    #[snafu(display("vk_helper::Buffer creation error: {}", source))]
    VkHelperBufferNew { source: VkHelperError },
    #[snafu(display("vk_helper::Buffer::get_allocation error: {}", source))]
    VkHelperGetAllocation { source: VkHelperError },
    #[snafu(display("vk_helper::DescriptorSets building error: {}", source))]
    VkHelperDescriptorSetNew { source: VkHelperError },
    #[snafu(display("vk_helper::DescriptorSets write sampled image error: {}", source))]
    VkHelperDescriptorSetWriteSampledImage { source: VkHelperError },
    #[snafu(display("vk_helper::DescriptorSets write sampler error: {}", source))]
    VkHelperDescriptorSetWriteSampler { source: VkHelperError },
    #[snafu(display("vk_helper::DescriptorSets write image error: {}", source))]
    VkHelperDescriptorSetWriteImage { source: VkHelperError },
    #[snafu(display("hazel::BuildPipelineCreateInfo error: {}", source))]
    HazelBuildPipelineCreateInfo { source: super::pipeline::Error },
    #[snafu(display("vk_helper::DescriptorSets write buffer error: {}", source))]
    VkHelperDescriptorSetsWriteBuffer { source: VkHelperError },
    #[snafu(display("vk_helper::CommandBuffer bind descriptor sets error: {}", source))]
    VkHelperCommandBufferBindDescriptorSets { source: VkHelperError },
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug)]
pub enum VkHelperUser<T: UserDataTrait> {
    Pipeline {
        user: T,
        push_size: usize,
        push_constant_ranges: HashMap<usize, (String, vk::ShaderStageFlags, usize)>,
        uniform_buffer_size: u32,
        uniform_buffer_layouts: Box<[Box<[Option<(u32, u32)>]>]>,
        type_data_by_name: HashMap<String, ShaderVariableType>,
        descriptor_set_layouts: DescriptorSetLayouts<T>,
    },
    None(T),
}

impl<T: UserDataTrait> std::default::Default for VkHelperUser<T> {
    fn default() -> Self {
        Self::None(T::default())
    }
}

#[derive(Clone)]
pub struct VertexArray<T: UserDataTrait> {
    pub vertexs: Vec<super::VertexBuffer<T>>,
    pub index: super::IndexBuffer<T>,
    pub attributes: HashMap<String, Vec<vk::VertexInputAttributeDescription>>,
    pub pipelines: HashMap<String, PipelineData<T>>,
    vertexs_mut: bool,
}

#[derive(Clone)]
pub struct VertexArrayBuilder<T: UserDataTrait> {
    pub vertexs: Vec<super::VertexBuffer<T>>,
    pub index: Option<super::IndexBuffer<T>>,
    pub attributes: HashMap<String, Vec<vk::VertexInputAttributeDescription>>,
    pub pipelines: HashMap<String, PipelineData<T>>,
    vertexs_mut: bool,
}

impl<T: UserDataTrait> Default for VertexArrayBuilder<T> {
    fn default() -> Self {
        Self {
            vertexs: Default::default(),
            index: Default::default(),
            pipelines: Default::default(),
            attributes: Default::default(),
            vertexs_mut: true,
        }
    }
}

impl<T: UserDataTrait> VertexArrayBuilder<T> {
    pub fn add_vertex_buffer(&mut self, buffer: VertexBuffer<T>) -> &mut Self {
        assert!(self.vertexs_mut);
        self.vertexs.push(buffer);
        self
    }

    pub fn set_index_buffer(&mut self, index: IndexBuffer<T>) -> &mut Self {
        self.index = Some(index);
        self
    }

    pub fn add_attribute(
        &mut self,
        name: &str,
        attributes: Vec<vk::VertexInputAttributeDescription>,
    ) -> &mut Self {
        self.attributes.insert(name.to_owned(), attributes);
        self
    }

    pub fn build(&self) -> Option<VertexArray<T>> {
        Some(VertexArray {
            vertexs: self.vertexs.clone(),
            index: self.index.as_ref()?.clone(),
            pipelines: self.pipelines.clone(),
            attributes: self.attributes.clone(),
            vertexs_mut: self.vertexs_mut,
        })
    }

    pub fn build_pipeline(
        &mut self,
        renderer: &Renderer<T>,
        input: &ShaderStagesInput,
        topology: vk::PrimitiveTopology,
        user: T,
    ) -> PipelineResult<(String, PipelineData<T>)> {
        self.vertexs_mut = false;
        let viewport_ref = renderer.viewport.borrow();
        let viewport = viewport_ref.as_ref().unwrap();
        let viwports = viewport.viewports;
        let scissors = viewport.scissors.clone();
        let viewport_state_info = vk::PipelineViewportStateCreateInfo::builder()
            .viewports(slice::from_ref(&viwports))
            .scissors(&scissors);
        build_pipeline(
            renderer.get_device().clone(),
            &viewport_state_info,
            viewport.get_render_pass().clone(),
            &self.vertexs,
            &self.attributes,
            input,
            topology,
            user,
        )
    }

    pub fn add_pipeline_data(&mut self, name: &str, pipeline_data: PipelineData<T>) -> &mut Self {
        self.pipelines.insert(String::from(name), pipeline_data);
        self
    }

    pub fn add_pipeline(
        &mut self,
        renderer: &Renderer<T>,
        input: &ShaderStagesInput,
        topology: vk::PrimitiveTopology,
        user: T,
    ) -> PipelineResult<&mut Self> {
        let (name, pipeline_data) = self.build_pipeline(renderer, input, topology, user)?;
        Ok(self.add_pipeline_data(&name, pipeline_data))
    }
}

impl<T: UserDataTrait> VertexArray<T> {
    pub fn new(index: IndexBuffer<T>) -> Self {
        Self {
            vertexs: Default::default(),
            index,
            pipelines: Default::default(),
            attributes: Default::default(),
            vertexs_mut: true,
        }
    }

    pub fn add_vertex_buffer(&mut self, buffer: VertexBuffer<T>) -> &mut Self {
        assert!(self.vertexs_mut);
        self.vertexs.push(buffer);
        self
    }

    pub fn set_index_buffer(&mut self, index: IndexBuffer<T>) -> &mut Self {
        self.index = index;
        self
    }

    pub fn add_attribute(
        &mut self,
        name: &str,
        attributes: Vec<vk::VertexInputAttributeDescription>,
    ) -> &mut Self {
        self.attributes.insert(name.to_owned(), attributes);
        self
    }

    pub fn build_pipeline(
        &mut self,
        renderer: &Renderer<T>,
        input: &ShaderStagesInput,
        topology: vk::PrimitiveTopology,
        user: T,
    ) -> PipelineResult<(String, PipelineData<T>)> {
        self.vertexs_mut = false;
        let viewport_ref = renderer.viewport.borrow();
        let viewport = viewport_ref.as_ref().unwrap();
        let viwports = viewport.viewports;
        let scissors = viewport.scissors.clone();
        let viewport_state_info = vk::PipelineViewportStateCreateInfo::builder()
            .viewports(slice::from_ref(&viwports))
            .scissors(&scissors);
        build_pipeline(
            renderer.get_device().clone(),
            &viewport_state_info,
            viewport.get_render_pass().clone(),
            &self.vertexs,
            &self.attributes,
            input,
            topology,
            user,
        )
    }

    pub fn add_pipeline_data(&mut self, name: &str, pipeline_data: PipelineData<T>) -> &mut Self {
        self.pipelines.insert(String::from(name), pipeline_data);
        self
    }

    pub fn add_pipeline(
        &mut self,
        renderer: &Renderer<T>,
        input: &ShaderStagesInput,
        topology: vk::PrimitiveTopology,
        user: T,
    ) -> PipelineResult<&mut Self> {
        let (name, pipeline_data) = self.build_pipeline(renderer, input, topology, user)?;
        Ok(self.add_pipeline_data(&name, pipeline_data))
    }

    pub(crate) fn resize(&self, renderer: &Renderer<T>) -> Result<()> {
        let viewport_ref = renderer.viewport.borrow();
        let viewport = viewport_ref.as_ref().unwrap();
        let viwports = viewport.viewports;
        let scissors = viewport.scissors.clone();
        let viewport_state_info = vk::PipelineViewportStateCreateInfo::builder()
            .viewports(slice::from_ref(&viwports))
            .scissors(&scissors);
        let render_pass = viewport.get_render_pass();
        for v in self.pipelines.values() {
            let pipeline_layout = v.pipeline.borrow().layout.clone();
            let user = v.pipeline.borrow().user.clone();
            v.pipeline.replace(
                build_pipeline_create_info(
                    &viewport_state_info,
                    render_pass.clone(),
                    &self.vertexs,
                    &v.attributes,
                    pipeline_layout,
                    v.shaders.clone(),
                    v.topology,
                    user,
                )
                .context(HazelBuildPipelineCreateInfoSnafu {})?,
            );
        }
        Ok(())
    }

    pub fn get_pipeline(&self, pipeline: &str) -> Pipeline<VkHelperUser<T>> {
        self.pipelines[pipeline].pipeline.borrow().clone()
    }
}

impl<T: UserDataTrait> Renderer<T> {
    pub fn create_vertex_buffer<V: Copy>(
        &self,
        stride: u32,
        vertex_data: &[V],
    ) -> Result<buffer::VertexBuffer<T>> {
        let size_of_data = size_of_val(vertex_data);
        let create_info = vk::BufferCreateInfo::builder()
            .size(size_of_data as _)
            .usage(vk::BufferUsageFlags::VERTEX_BUFFER);

        let vertex_buffer = Buffer::new(
            self.allocator.clone(),
            &create_info,
            &vk_mem::AllocationCreateInfo {
                usage: vk_mem::MemoryUsage::Auto,
                flags: vk_mem::AllocationCreateFlags::MAPPED
                    | vk_mem::AllocationCreateFlags::HOST_ACCESS_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            Default::default(),
        )
        .context(VkHelperBufferNewSnafu {})?;

        let mut vertex_align = vertex_buffer
            .get_allocation()
            .context(VkHelperGetAllocationSnafu {})?;
        vertex_align.copy_from_slice(vertex_data);

        Ok(buffer::VertexBuffer {
            inner: vertex_buffer,
            stride,
        })
    }

    pub fn create_index_buffer(&self, index_data: &[u32]) -> Result<buffer::IndexBuffer<T>> {
        let size_of_data = size_of_val(index_data);
        let create_info = vk::BufferCreateInfo::builder()
            .size(size_of_data as _)
            .usage(vk::BufferUsageFlags::INDEX_BUFFER);

        let index_buffer = Buffer::new(
            self.allocator.clone(),
            &create_info,
            &vk_mem::AllocationCreateInfo {
                usage: vk_mem::MemoryUsage::Auto,
                flags: vk_mem::AllocationCreateFlags::MAPPED
                    | vk_mem::AllocationCreateFlags::HOST_ACCESS_SEQUENTIAL_WRITE,
                ..Default::default()
            },
            Default::default(),
        )
        .context(VkHelperBufferNewSnafu {})?;

        let mut index_align = index_buffer
            .get_allocation()
            .context(VkHelperGetAllocationSnafu {})?;
        index_align.copy_from_slice(index_data);

        Ok(buffer::IndexBuffer {
            inner: index_buffer,
            count: index_data.len() as u32,
        })
    }

    pub fn create_image() {
        todo!()
    }
}

/* todo
use std::path::PathBuf;
#[derive(Clone)]
pub enum ImageInput {
    Data(Vec<u8>),
    File(PathBuf),
}
 */

fn write_variable<T: UserDataTrait>(uniform: &mut [u8], value: &PipelineVariableValue<T>) {
    match value {
        PipelineVariableValue::Sampler2D(_, _) => unimplemented!(),
        PipelineVariableValue::VecF32(data) => {
            // assert_eq!(uniform.len(), size_of_val(data));
            uniform.copy_from_slice(data.as_bytes());
        }
        PipelineVariableValue::Mat4(mat4) => {
            assert_eq!(uniform.len(), 64);
            for (x, y) in uniform
                .iter_mut()
                .zip(mat4.iter().copied().flat_map(f32::to_ne_bytes))
            {
                *x = y;
            }
        }
        _ => unimplemented!(),
    };
}

fn write_variable_to_iter<'a>(
    uniform: impl Iterator<Item = &'a mut u8>,
    value: &PipelineVariableValue<impl UserDataTrait>,
) {
    match value {
        PipelineVariableValue::Sampler2D(_, _) => unimplemented!(),
        PipelineVariableValue::VecF32(data) => {
            for (x, y) in uniform.zip(data.as_bytes()) {
                *x = *y;
            }
        }
        PipelineVariableValue::Mat4(mat4) => {
            for (x, y) in uniform.zip(mat4.iter().copied().flat_map(f32::to_ne_bytes)) {
                *x = y;
            }
        }
        _ => unimplemented!(),
    };
}

pub struct Package<T: UserDataTrait> {
    vertex_array: Rc<VertexArray<T>>,
    pipeline_layout: PipelineLayout<render::VkHelperUser<T>>,
    uniform_buffer: Buffer<VkHelperUser<T>>,
    push_constant: Box<[u8]>,
    descriptor_sets: Box<[DescriptorSet<VkHelperUser<T>>]>,
    pipeline: Pipeline<VkHelperUser<T>>,
}

impl<T: UserDataTrait> Package<T> {
    pub fn bind(&self, command_buffer: &CommandBuffer<VkHelperUser<T>>) -> VkHelperUser<T> {
        let (buffers, offsets): (Vec<_>, Vec<_>) = (
            self.vertex_array
                .vertexs
                .iter()
                .map(|it| (**it).clone())
                .collect(),
            std::iter::repeat(0u64)
                .take(self.vertex_array.vertexs.len())
                .collect(),
        );
        command_buffer.bind_vertex_buffers(0, buffers, &offsets);
        self.vertex_array.index.bind(command_buffer);
        command_buffer.bind_pipeline(self.pipeline.clone());
        self.pipeline.user.clone()
    }
}

impl<T: UserDataTrait> Renderer<T> {
    pub fn package(
        &self,
        pipeline: &str,
        pipeline_variables: &HashMap<String, PipelineVariableValue<T>>,
        vertex_array: Rc<VertexArray<T>>,
    ) -> Result<Package<T>> {
        let pipeline = vertex_array.get_pipeline(pipeline);
        if let (
            VkHelperUser::<T>::Pipeline {
                push_size,
                uniform_buffer_size,
                uniform_buffer_layouts,
                type_data_by_name,
                descriptor_set_layouts,
                ..
            },
            pipeline_layout,
        ) = (pipeline.user.clone(), pipeline.layout.clone())
        {
            let mut push_constant: Box<_> = std::iter::repeat(0u8).take(push_size as _).collect();

            let create_info = vk::BufferCreateInfo::builder()
                .size(u64::from(uniform_buffer_size))
                .usage(vk::BufferUsageFlags::UNIFORM_BUFFER);
            let uniform_buffer = Buffer::new(
                self.allocator.clone(),
                &create_info,
                &vk_mem::AllocationCreateInfo {
                    usage: vk_mem::MemoryUsage::Auto,
                    flags: vk_mem::AllocationCreateFlags::MAPPED
                        | vk_mem::AllocationCreateFlags::HOST_ACCESS_SEQUENTIAL_WRITE,
                    ..Default::default()
                },
                Default::default(),
            )
            .context(VkHelperBufferNewSnafu {})?;

            let descriptor_sets = DescriptorSet::new(
                &self.descriptor_pool,
                &descriptor_set_layouts.0,
                vk::DescriptorSetAllocateInfo::builder(),
                Default::default(),
                vec![],
            )
            .context(VkHelperDescriptorSetNewSnafu {})?;

            for (name, type_data) in type_data_by_name.clone() {
                match type_data {
                    ShaderVariableType::Push { offset, range } => {
                        write_variable(
                            &mut push_constant[offset..offset + range],
                            pipeline_variables
                                .get(&name)
                                .unwrap_or_else(|| &self.background_variables[&name]),
                        );
                    }
                    ShaderVariableType::UniformBufferMember {
                        set,
                        binding,
                        offset,
                        size,
                    } => write_variable_to_iter(
                        uniform_buffer
                            .get_allocation()
                            .context(VkHelperGetAllocationSnafu)?
                            .iter_mut()
                            .skip(
                                uniform_buffer_layouts[set as usize][binding as usize]
                                    .unwrap()
                                    .0 as usize
                                    + offset as usize,
                            )
                            .take(size as usize),
                        pipeline_variables
                            .get(&name)
                            .unwrap_or_else(|| &self.background_variables[&name]),
                    ),
                    ShaderVariableType::UniformSampler2D { set, binding } => {
                        if let PipelineVariableValue::Sampler2D(sampler, image) =
                            pipeline_variables[&name].clone()
                        {
                            descriptor_sets[set as usize]
                                .write_image(
                                    sampler,
                                    image.0,
                                    vk::DescriptorImageInfo::builder()
                                        .image_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                                    vk::WriteDescriptorSet::builder().dst_binding(binding),
                                )
                                .context(VkHelperDescriptorSetWriteSampledImageSnafu {})?;
                        } else {
                            unimplemented!();
                        };
                    }
                    ShaderVariableType::UniformSampler { set, binding } => {
                        if let PipelineVariableValue::Sampler(sampler) =
                            pipeline_variables[&name].clone()
                        {
                            descriptor_sets[set as usize]
                                .write_sampler(
                                    sampler,
                                    vk::DescriptorImageInfo::builder(),
                                    vk::WriteDescriptorSet::builder().dst_binding(binding),
                                )
                                .context(VkHelperDescriptorSetWriteSamplerSnafu {})?;
                        } else {
                            unimplemented!();
                        };
                    }
                    ShaderVariableType::UniformTexture2DArray { set, binding } => {
                        if let PipelineVariableValue::Textures(image_views) =
                            pipeline_variables[&name].clone()
                        {
                            let images = image_views
                                .into_iter()
                                .map(|image| {
                                    (
                                        image,
                                        vk::DescriptorImageInfo::builder().image_layout(
                                            vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                                        ),
                                    )
                                })
                                .collect::<Vec<_>>();
                            descriptor_sets[set as usize]
                                .write_images(
                                    images,
                                    vk::WriteDescriptorSet::builder().dst_binding(binding),
                                )
                                .context(VkHelperDescriptorSetWriteImageSnafu {})?;
                        } else {
                            unimplemented!();
                        };
                    }
                };
            }
            uniform_buffer_layouts
                .iter()
                .enumerate()
                .map(|(set, layouts)| {
                    layouts
                        .iter()
                        .enumerate()
                        .filter_map(|(binding, layout)| {
                            layout.map(|(offset, range)| (binding, offset, range))
                        })
                        .try_for_each(|(binding, offset, range)| {
                            descriptor_sets[set as usize]
                                .write_buffer(
                                    uniform_buffer.clone(),
                                    vk::DescriptorBufferInfo::builder()
                                        .offset(u64::from(offset))
                                        .range(u64::from(range)),
                                    vk::WriteDescriptorSet::builder().dst_binding(binding as _),
                                )
                                .context(VkHelperDescriptorSetsWriteBufferSnafu {})
                        })
                })
                .collect::<Result<Vec<()>>>()?;

            Ok(Package {
                vertex_array,
                pipeline_layout,
                uniform_buffer,
                push_constant,
                descriptor_sets,
                pipeline,
            })
        } else {
            Err(todo!())
        }
    }

    pub fn submit(
        &self,
        pipeline_variables: &HashMap<String, PipelineVariableValue<T>>,
        command_buffer: &CommandBuffer<VkHelperUser<T>>,
        package: &mut Package<T>,
    ) -> Result<()> {
        if let VkHelperUser::<T>::Pipeline {
            push_constant_ranges,
            uniform_buffer_layouts,
            type_data_by_name,
            ..
        } = package.bind(command_buffer)
        {
            for (name, value) in pipeline_variables.iter() {
                match type_data_by_name[name] {
                    ShaderVariableType::Push { offset, range } => {
                        write_variable(&mut package.push_constant[offset..offset + range], value);
                    }
                    ShaderVariableType::UniformBufferMember {
                        set,
                        binding,
                        offset,
                        size,
                    } => write_variable_to_iter(
                        package
                            .uniform_buffer
                            .get_allocation()
                            .context(VkHelperGetAllocationSnafu)?
                            .iter_mut()
                            .skip(
                                uniform_buffer_layouts[set as usize][binding as usize]
                                    .unwrap()
                                    .0 as usize
                                    + offset as usize,
                            )
                            .take(size as usize),
                        value,
                    ),
                    ShaderVariableType::UniformSampler2D { set, binding } => {
                        if let PipelineVariableValue::Sampler2D(sampler, image) = value.clone() {
                            package.descriptor_sets[set as usize]
                                .write_image(
                                    sampler,
                                    image.0,
                                    vk::DescriptorImageInfo::builder()
                                        .image_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL),
                                    vk::WriteDescriptorSet::builder().dst_binding(binding),
                                )
                                .context(VkHelperDescriptorSetWriteSampledImageSnafu {})?;
                        } else {
                            unimplemented!();
                        };
                    }
                    ShaderVariableType::UniformSampler { set, binding } => {
                        if let PipelineVariableValue::Sampler(sampler) = value.clone() {
                            package.descriptor_sets[set as usize]
                                .write_sampler(
                                    sampler,
                                    vk::DescriptorImageInfo::builder(),
                                    vk::WriteDescriptorSet::builder().dst_binding(binding),
                                )
                                .context(VkHelperDescriptorSetWriteSamplerSnafu {})?;
                        } else {
                            unimplemented!();
                        };
                    }
                    ShaderVariableType::UniformTexture2DArray { set, binding } => {
                        if let PipelineVariableValue::Textures(image_views) = value.clone() {
                            let images = image_views
                                .into_iter()
                                .map(|image| {
                                    (
                                        image,
                                        vk::DescriptorImageInfo::builder().image_layout(
                                            vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                                        ),
                                    )
                                })
                                .collect::<Vec<_>>();
                            package.descriptor_sets[set as usize]
                                .write_images(
                                    images,
                                    vk::WriteDescriptorSet::builder().dst_binding(binding),
                                )
                                .context(VkHelperDescriptorSetWriteImageSnafu {})?;
                        } else {
                            unimplemented!();
                        };
                    }
                };
            }
            push_constant_ranges
                .iter()
                .for_each(|(offset, (_, stage_flags, size))| {
                    let end = (offset + size) as usize;
                    unsafe {
                        self.get_device().cmd_push_constants(
                            ***command_buffer,
                            **package.pipeline_layout,
                            *stage_flags,
                            *offset as _,
                            &package.push_constant[*offset as usize..end],
                        )
                    }
                });

            command_buffer
                .bind_descriptor_sets(0, &package.descriptor_sets, &[])
                .context(VkHelperCommandBufferBindDescriptorSetsSnafu {})?;

            unsafe {
                self.get_device().cmd_draw_indexed(
                    ***command_buffer,
                    package.vertex_array.index.count,
                    1,
                    0,
                    0,
                    0,
                );
            };
        }
        Ok(())
    }
}
