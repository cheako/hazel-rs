// Copyright 2020 Michael Mestnik

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![feature(cell_update)]
#![feature(result_option_inspect)]

use std::cell::{Cell, RefCell};
use std::fmt::Debug;
use std::rc::Rc;
use std::slice;
use std::time::{Duration, Instant};

pub mod camera;
pub mod imgui;
pub mod maths;
pub mod reexport;
pub mod renderer;
use renderer::base::NextImage;
use renderer::{UserDataTrait, VkHelperUser};
pub mod window;

pub mod input {
    use super::dpi::PhysicalPosition;
    use crate::event::VirtualKeyCode;
    use std::collections::HashMap;

    pub struct Input {
        pub keys: HashMap<VirtualKeyCode, u8>,
        pub buttons: [u8; 8],
        pub position: PhysicalPosition<f64>,
    }

    impl Default for Input {
        fn default() -> Self {
            Self {
                keys: Default::default(),
                buttons: Default::default(),
                position: PhysicalPosition { x: 0.0, y: 0.0 },
            }
        }
    }

    impl Input {
        pub fn get_key(&self, key: VirtualKeyCode) -> bool {
            self.keys.get(&key).map_or(false, |v| *v != 0)
        }

        pub fn get_button(&self, button: u8) -> bool {
            self.buttons[button as usize] != 0
        }

        pub(crate) fn key_press(&mut self, key: VirtualKeyCode) {
            let x = self.keys.entry(key).or_insert(0);
            *x += 1;
        }
        pub(crate) fn key_release(&mut self, key: VirtualKeyCode) {
            self.keys
                .get_mut(&key)
                .iter_mut()
                .filter(|&&mut &mut x| x != 0)
                .for_each(|x| **x -= 1);
        }
        pub(crate) fn button_press(&mut self, button: u8) {
            self.buttons[button as usize] += 1;
        }
        pub(crate) fn button_release(&mut self, button: u8) {
            self.buttons[button as usize] -= 1;
        }
    }
}

use reexport::ash::vk;
use reexport::ash_tray::vk_helper::{BinarySemaphore, CommandBuffer};
pub use reexport::winit::event_loop::{ControlFlow, EventLoopWindowTarget};
pub use reexport::winit::{dpi, event, window::Window as WinitWindow};

/* reexport macros */
pub use ash_tray::{
    glsl_cs, glsl_fs, glsl_gs, glsl_tcs, glsl_tes, glsl_vs, include_glsl_cs, include_glsl_fs,
    include_glsl_gs, include_glsl_tcs, include_glsl_tes, include_glsl_vs, vk_api_version_major,
    vk_api_version_minor, vk_api_version_patch, vk_make_api_version, GLSLEmbedImpl,
};

use crate::reexport::ash_tray::vk_helper::Error as VkHelperError;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("pipeline creation error: {}", source))]
    VkHelperWaitAndReset { source: VkHelperError },
    #[snafu(display("vk::reset_command_buffer() error: {}", source))]
    VkResetCommandBuffer { source: vk::Result },
    #[snafu(display("vk::begin_command_buffer() error: {}", source))]
    VkBeginCommandBuffer { source: vk::Result },
    #[snafu(display("vk::end_command_buffer() error: {}", source))]
    VkEndCommandBuffer { source: vk::Result },
    #[snafu(display("vk::queue_submit() error: {}", source))]
    VkHelperQueueSubmit { source: VkHelperError },
}

pub type Result<T> = std::result::Result<T, Error>;

pub struct AppAPI<UserEvent: 'static, UserData: UserDataTrait> {
    pub log: reexport::slog::Logger,
    #[allow(clippy::type_complexity)]
    pub layers: RefCell<Vec<Vec<Rc<RefCell<dyn Layer<UserEvent, UserData>>>>>>,
    pub renderer: renderer::Renderer<UserData>,
    pub input: RefCell<input::Input>,
    pub ts: Cell<Instant>,
    pub elapsed: Cell<Duration>,
    pub resized: Cell<bool>,
    pub resize_ctr: RefCell<Option<(u32, Instant)>>,
}

impl<UserEvent: 'static, UserData: UserDataTrait> AppAPI<UserEvent, UserData> {
    pub fn get_key(&self, key: crate::event::VirtualKeyCode) -> bool {
        self.input.borrow().get_key(key)
    }
}

pub trait Layer<UserEvent: 'static, UserData: UserDataTrait> {
    fn get_name(&mut self) -> String {
        "".to_owned()
    }

    /// Before pipelines are recreated or sizes are updated.
    fn on_preresize(
        &mut self,
        _api: &AppAPI<UserEvent, UserData>,
        _s: dpi::PhysicalSize<u32>,
        _w: &EventLoopWindowTarget<UserEvent>,
        _c: &mut ControlFlow,
    ) {
    }

    /// After command buffer is ready, but prior to begin render pass.
    /// Essential for loading textures or copying data GPU arround.    
    fn on_preupdate(
        &mut self,
        _api: &AppAPI<UserEvent, UserData>,
        _command_buffer: &CommandBuffer<VkHelperUser<UserData>>,
        _w: &EventLoopWindowTarget<UserEvent>,
        _c: &mut ControlFlow,
    ) {
    }

    fn on_imgui_update(&mut self, _ui: &crate::imgui::Ui) {}

    fn on_update(
        &mut self,
        _api: &AppAPI<UserEvent, UserData>,
        _command_buffer: &CommandBuffer<VkHelperUser<UserData>>,
        _w: &EventLoopWindowTarget<UserEvent>,
        _c: &mut ControlFlow,
    ) {
    }

    fn on_event(
        &mut self,
        _api: &AppAPI<UserEvent, UserData>,
        _e: &event::Event<UserEvent>,
        _w: &EventLoopWindowTarget<UserEvent>,
        _c: &mut ControlFlow,
    ) -> bool {
        false
    }
}

pub struct Application<UserEvent: 'static, UserData: UserDataTrait> {
    pub log: reexport::slog::Logger,
    pub window: window::Window<UserEvent>,
    #[allow(clippy::type_complexity)]
    pub layers: Vec<Vec<Rc<RefCell<dyn Layer<UserEvent, UserData>>>>>,
}

impl<UserEvent: 'static, UserData: UserDataTrait> std::fmt::Debug
    for Application<UserEvent, UserData>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "")
    }
}

impl<UserEvent: 'static, UserData: UserDataTrait> Default for Application<UserEvent, UserData> {
    fn default() -> Self {
        use reexport::slog::{o, Drain};
        use reexport::{slog as _slog, slog_async as _slog_async, slog_term as _slog_term};

        Application {
            log: _slog::Logger::root(
                _slog_async::Async::default(_slog_term::term_full().fuse()).fuse(),
                o!(),
            ),
            window: Default::default(),
            layers: Default::default(),
        }
    }
}

fn begin_pipeline<UserEvent: 'static, UserData: UserDataTrait>(
    api: &AppAPI<UserEvent, UserData>,
    command_buffer: &CommandBuffer<VkHelperUser<UserData>>,
) -> Result<()> {
    if api.renderer.frame_ctr.get() > 0 {
        command_buffer
            .wait_and_reset(std::u64::MAX)
            .context(VkHelperWaitAndResetSnafu {})?;
    }
    api.renderer.frame_ctr.update(|x| x + 1);

    unsafe {
        api.renderer.get_device().reset_command_buffer(
            ***command_buffer,
            crate::reexport::ash::vk::CommandBufferResetFlags::RELEASE_RESOURCES,
        )
    }
    .context(VkResetCommandBufferSnafu {})?;

    let begin_info = crate::reexport::ash::vk::CommandBufferBeginInfo::builder()
        .flags(crate::reexport::ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

    unsafe {
        api.renderer
            .get_device()
            .begin_command_buffer(***command_buffer, &begin_info)
    }
    .context(VkBeginCommandBufferSnafu {})?;

    Ok(())
}

fn queue_submit<UserEvent: 'static, UserData: UserDataTrait>(
    api: &AppAPI<UserEvent, UserData>,
    wait: Option<BinarySemaphore<renderer::VkHelperUser<UserData>>>,
    next_image: &NextImage<UserData>,
) -> Result<()> {
    unsafe {
        api.renderer
            .get_device()
            .end_command_buffer(**next_image.command_buffer)
    }
    .context(VkEndCommandBufferSnafu {})?;

    let ctr = api.renderer.frame_ctr.get() << 2;

    let (timeline_ctr, submit_info, wait_semaphores) = if let Some(wait_semaphores) = wait {
        (
            ctr + 5,
            vk::SubmitInfo::builder().wait_dst_stage_mask(slice::from_ref(
                &vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            )),
            vec![wait_semaphores],
        )
    } else {
        (ctr + 7, vk::SubmitInfo::builder(), vec![])
    };

    dbg!(timeline_ctr);
    next_image.command_buffer.set_signal_value(timeline_ctr);
    next_image
        .command_buffer
        .queue_submit(
            api.renderer.present_queue.clone(),
            vec![],
            wait_semaphores,
            vec![],
            vec![next_image.rendering_complete.clone()],
            submit_info,
        )
        .context(VkHelperQueueSubmitSnafu {})?;

    Ok(())
}

fn run<UserEvent: 'static, UserData: UserDataTrait>(
    api: &AppAPI<UserEvent, UserData>,
    e: event::Event<UserEvent>,
    w: &EventLoopWindowTarget<UserEvent>,
    c: &mut ControlFlow,
) {
    match e {
        event::Event::WindowEvent {
            event: event::WindowEvent::CloseRequested,
            ..
        } => *c = ControlFlow::Exit,
        event::Event::MainEventsCleared => {
            if api.resized.get() {
                api.resize_ctr
                    .borrow_mut()
                    .get_or_insert((0, Instant::now()));
                api.resize_ctr
                    .borrow_mut()
                    .iter_mut()
                    .for_each(|(ctr, _)| *ctr += 1);
                api.resized.set(false);
            } else {
                api.resize_ctr.replace(None);
            }
            match &mut *api.resize_ctr.borrow_mut() {
                None => api.renderer.window.request_redraw(),
                Some((_, ts)) => {
                    let t = ts.elapsed();
                    if t.as_millis() > 100_000 {
                        api.renderer.window.request_redraw();
                        *ts += t;
                    }
                }
            }
        }
        _ => {}
    }

    let surface_resolution = api
        .renderer
        .viewport
        .borrow()
        .as_ref()
        .unwrap()
        .surface_resolution;
    let layers = api.layers.borrow().clone();
    match e {
        event::Event::WindowEvent {
            event: event::WindowEvent::Resized(size),
            ..
        } => {
            if size.width as u32 == surface_resolution.width
                && size.height as u32 == surface_resolution.height
            {
                return;
            };

            api.resized.set(true);
            layers.iter().for_each(|it| {
                it.iter()
                    .for_each(|it| it.borrow_mut().on_preresize(api, size, w, c))
            });
            /* Swapchain MUST be destroyed prior to crating a new one. */
            let (command_pool, timeline_semaphore) =
                api.renderer.viewport.replace(None).unwrap().dissolve();

            api.renderer
                .viewport
                .replace(Some(renderer::RendererViewport::new(
                    &command_pool,
                    &timeline_semaphore,
                    api.renderer.surface.clone(),
                    api.renderer.allocator.clone(),
                    api.renderer.present_queue.clone(),
                    size.width as _,
                    size.height as _,
                )));

            api.renderer.resize_pipelines().unwrap();
            /* Todo: Fixme */
            for it in layers.iter().rev() {
                for it in it.iter().rev() {
                    if it.borrow_mut().on_event(api, &e, w, c) {
                        return;
                    };
                }
            }
        }
        event::Event::RedrawRequested(_) => {
            use ash_tray::vk_helper::Error::VkAcquireNextImages;

            api.elapsed.set(api.ts.get().elapsed());
            api.ts.update(|ts| ts + api.elapsed.get());

            let ret = api
                .renderer
                .viewport
                .borrow()
                .as_ref()
                .unwrap()
                .swapchain
                .acquire_next_image(50_000_000u64 /* 0.05s, 20hz */);

            match ret {
                Ok((present_index, _, semaphore)) => {
                    let next_image = api.renderer.viewport.borrow();
                    let next_image =
                        &next_image.as_ref().unwrap().next_images[present_index as usize];
                    begin_pipeline(api, &next_image.command_buffer).unwrap();

                    layers.iter().for_each(|it| {
                        it.iter().for_each(|it| {
                            it.borrow_mut()
                                .on_preupdate(api, &next_image.command_buffer, w, c)
                        })
                    });

                    let clear_values = [
                        vk::ClearValue {
                            color: vk::ClearColorValue {
                                float32: [0.0, 0.0, 0.0, 0.0],
                            },
                        },
                        vk::ClearValue {
                            depth_stencil: vk::ClearDepthStencilValue {
                                depth: 1.0, // Might as well be Infinity.
                                stencil: 0,
                            },
                        },
                    ];

                    let render_pass_begin_info = vk::RenderPassBeginInfo::builder()
                        .render_area(vk::Rect2D {
                            offset: vk::Offset2D { x: 0, y: 0 },
                            extent: api
                                .renderer
                                .viewport
                                .borrow()
                                .as_ref()
                                .unwrap()
                                .surface_resolution,
                        })
                        .clear_values(&clear_values);

                    next_image
                        .command_buffer
                        .begin_render_pass(next_image.inner.clone(), render_pass_begin_info);
                    unsafe {
                        api.renderer.get_device().cmd_set_viewport(
                            **next_image.command_buffer,
                            0,
                            slice::from_ref(
                                &api.renderer.viewport.borrow().as_ref().unwrap().viewports,
                            ),
                        );
                        api.renderer.get_device().cmd_set_scissor(
                            **next_image.command_buffer,
                            0,
                            &api.renderer.viewport.borrow().as_ref().unwrap().scissors,
                        );
                    }

                    layers.iter().for_each(|it| {
                        it.iter().for_each(|it| {
                            it.borrow_mut()
                                .on_update(api, &next_image.command_buffer, w, c)
                        })
                    });
                    unsafe {
                        api.renderer
                            .get_device()
                            .cmd_end_render_pass(**next_image.command_buffer);
                    };
                    queue_submit(api, Some(semaphore), &next_image).unwrap();
                    let present_info = vk::PresentInfoKHR::builder()
                        .image_indices(slice::from_ref(&present_index));
                    api.renderer
                        .viewport
                        .borrow()
                        .as_ref()
                        .unwrap()
                        .swapchain
                        .queue_present(vec![next_image.rendering_complete.clone()], present_info)
                        .unwrap();
                }
                Err(VkAcquireNextImages {
                    source: vk::Result::ERROR_OUT_OF_DATE_KHR,
                }) => {}
                Err(VkAcquireNextImages {
                    source: vk::Result::TIMEOUT,
                }) => {
                    panic!("Cheako said \"It's time to close,\" Good Night.");
                }
                Err(VkAcquireNextImages { source: e }) => Err(e).unwrap(),
                Err(_) => unreachable!(),
            };
        }
        event::Event::WindowEvent {
            event:
                event::WindowEvent::KeyboardInput {
                    input:
                        event::KeyboardInput {
                            state: event::ElementState::Pressed,
                            virtual_keycode: Some(virtual_keycode),
                            ..
                        },
                    ..
                },
            ..
        } => {
            api.input.borrow_mut().key_press(virtual_keycode);
            /* Todo: Fixme */
            for it in layers.iter().rev() {
                for it in it.iter().rev() {
                    if it.borrow_mut().on_event(api, &e, w, c) {
                        return;
                    };
                }
            }
        }
        event::Event::DeviceEvent {
            event:
                event::DeviceEvent::Key(event::KeyboardInput {
                    state: event::ElementState::Released,
                    virtual_keycode: Some(virtual_keycode),
                    ..
                }),
            ..
        } => {
            api.input.borrow_mut().key_release(virtual_keycode);
            /* Todo: Fixme */
            for it in layers.iter().rev() {
                for it in it.iter().rev() {
                    if it.borrow_mut().on_event(api, &e, w, c) {
                        return;
                    };
                }
            }
        }
        event::Event::WindowEvent {
            event:
                event::WindowEvent::MouseInput {
                    state: event::ElementState::Pressed,
                    button,
                    ..
                },
            ..
        } => {
            match button {
                event::MouseButton::Left => api.input.borrow_mut().button_press(0),
                event::MouseButton::Middle => api.input.borrow_mut().button_press(1),
                event::MouseButton::Right => api.input.borrow_mut().button_press(2),
                event::MouseButton::Other(button) => {
                    api.input.borrow_mut().button_press(button as u8 + 3)
                }
            }

            /* Todo: Fixme */
            for it in layers.iter().rev() {
                for it in it.iter().rev() {
                    if it.borrow_mut().on_event(api, &e, w, c) {
                        return;
                    };
                }
            }
        }
        event::Event::WindowEvent {
            event:
                event::WindowEvent::MouseInput {
                    state: event::ElementState::Released,
                    button,
                    ..
                },
            ..
        } => {
            match button {
                event::MouseButton::Left => api.input.borrow_mut().button_release(0),
                event::MouseButton::Middle => api.input.borrow_mut().button_release(1),
                event::MouseButton::Right => api.input.borrow_mut().button_release(2),
                event::MouseButton::Other(button) => {
                    api.input.borrow_mut().button_release(button as u8 + 3)
                }
            };
            /* Todo: Fixme */
            for it in layers.iter().rev() {
                for it in it.iter().rev() {
                    if it.borrow_mut().on_event(api, &e, w, c) {
                        return;
                    };
                }
            }
        }
        event::Event::WindowEvent {
            event: event::WindowEvent::CursorMoved { position, .. },
            ..
        } => {
            api.input.borrow_mut().position = position;
            /* Todo: Fixme */
            for it in layers.iter().rev() {
                for it in it.iter().rev() {
                    if it.borrow_mut().on_event(api, &e, w, c) {
                        return;
                    };
                }
            }
        }
        _ => {
            /* Todo: Fixme */
            for it in layers.iter().rev() {
                for it in it.iter().rev() {
                    if it.borrow_mut().on_event(api, &e, w, c) {
                        return;
                    };
                }
            }
        }
    }
}

impl<UserEvent: 'static, UserData: 'static + UserDataTrait> Application<UserEvent, UserData> {
    pub fn run(self) {
        if let window::Window::Full(w) = self.window {
            let renderer =
                renderer::Renderer::new(w.winit_window, 800, 600, vk_make_api_version(0, 0, 0, 0))
                    .unwrap();
            let api = AppAPI {
                log: self.log,
                layers: self.layers.into(),
                renderer,
                input: Default::default(),
                ts: Instant::now().into(),
                elapsed: Default::default(),
                resize_ctr: Default::default(),
                resized: Default::default(),
            };
            w.event_loop.run(move |e, w, c| run(&api, e, w, c));
        }
    }
}
