//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

// use std::convert::TryInto;
use std::ops::{Add, BitAnd, Shl};

use hazel_ge::im_str;
use hazel_ge::reexport::glm;
pub use hazel_ge::reexport::glm::Vec2;
use hazel_ge::reexport::imgui;

pub struct Node {
    pub east_north: bool,
    pub diagonal_north_west: bool,
    pub north_east: bool,
    pub north_west: bool,
    pub diagonal_north_east: bool,
    pub east_south: bool,
}

impl<T: BitAnd + Shl + Eq> From<T> for Node {
    fn from(v: T) -> Self {
        Self {
            east_north: v & 1 << 0 != 0,
            diagonal_north_west: v & 1 << 1 != 0,
            north_east: v & 1 << 2 != 0,
            north_west: v & 1 << 3 != 0,
            diagonal_north_east: v & 1 << 4 != 0,
            east_south: v & 1 << 5 != 0,
        }
    }
}

impl<T: Shl + Add> From<Node> for T {
    fn from(v: Node) -> Self {
        (v.east_north as Self)
            << 0 + (v.diagonal_north_west as Self)
            << 1 + (v.north_east as Self)
            << 2 + (v.north_west as Self)
            << 3 + (v.diagonal_north_east as Self)
            << 4 + (v.east_south as Self)
            << 5 + 0
    }
}

pub enum Direction {
    NorthEast,
    NorthWest,
    DiagonalNorthEast,
    EastNorth,
    EastSouth,
    DiagonalSouthEast,
    SouthEast,
    SouthWest,
    DiagonalSouthWest,
    WestNorth,
    WestSouth,
    DiagonalNorthWest,
}

pub enum Model {
    EngineTwo,
    EngineThree,
    FoodTwo,
    FoodThree,
    PaperSix,
    PaperSeven,
}

struct train {
    engine: Model,
    car: Car,
}

struct Car {
    model: Model,
    leg: (Direction, f32),
    opleg: Option<(Direction, f32)>,
}

pub struct State {
    pub t: f32,
    pub dt: f32,
    pub map: [[Node; 8]; 6],
    pub train: Train,
}

impl Default for State {
    fn default() -> Self {
        use Object::*;
        Self {
            t: 0.,
            dt: 1.,
            map: [
                [
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    8.into(),
                ],
                [
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    8.into(),
                    0.into(),
                ]
                .into(),
                [
                    0.into(),
                    0.into(),
                    0.into(),
                    16.into(),
                    0.into(),
                    8.into(),
                    0.into(),
                    0.into(),
                ]
                .into(),
                [
                    0.into(),
                    0.into(),
                    8.into(),
                    0.into(),
                    1.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                ]
                .into(),
                [
                    0.into(),
                    8.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                ]
                .into(),
                [
                    8.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                    0.into(),
                ]
                .into(),
            ],
            objects: vec![(
                EngineTwo,
                glm::vec2(2., 3.),
                vec![(FoodTwo, 1, 4), (PaperSix, 0, 5)],
            )],
        }
    }
}

impl hazel_ge::Layer<(), ()> for State {
    fn get_name(&mut self) -> String {
        "Train Timer".to_owned()
    }

    fn on_imgui_update(&mut self, ui: &hazel_ge::imgui::Ui) {
        if let Some(w) = imgui::Window::new(im_str!("Timer")).begin(&ui) {
            imgui::Slider::new(im_str!("time"), std::ops::RangeInclusive::new(0f32, 4.))
                .build(&ui, &mut self.t);
            // ui.text(im_str!("time: {}", self.t));
            if self.dt != 0. {
                if ui.button(im_str!("Pause"), [120f32, 30.]) {
                    self.dt = 0.;
                }
            } else {
                if ui.button(im_str!("Start"), [120f32, 30.]) {
                    self.dt = 1.;
                }
            }
            if ui.button(im_str!("Reset"), [120f32, 30.]) {
                self.t = 0.;
            }
            w.end(&ui);
        }
    }

    fn on_preupdate(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        use Object::*;
        self.t = (self.t + self.dt * api.elapsed.as_secs_f32()) % 4.;
        self.objects[0] = match (
            self.t,
            vec![
                match self.t {
                    t if t < 1.5 => (FoodTwo, 1, 4),
                    t if t < 2.5 => (
                        FoodThree,
                        glm::lerp(&glm::vec2(2.5, 2.5), &glm::vec2(4.5, 2.5), t - 1.5),
                    ),
                    t => (
                        FoodTwo,
                        glm::lerp(&glm::vec2(4.5, 2.5), &glm::vec2(6., 1.), (t - 2.5) / 1.5),
                    ),
                },
                match self.t {
                    t if t < 2.5 => (
                        PaperSix,
                        glm::lerp(&glm::vec2(0., 5.), &glm::vec2(2.5, 2.5), t / 2.5),
                    ),
                    t if t < 3.5 => (
                        PaperSeven,
                        glm::lerp(&glm::vec2(2.5, 2.5), &glm::vec2(4.5, 2.5), t - 2.5),
                    ),
                    t => (
                        PaperSix,
                        glm::lerp(&glm::vec2(4.5, 2.5), &glm::vec2(5., 2.), (t - 3.5) / 0.5),
                    ),
                },
            ],
        ) {
            (t, v) if t < 0.5 => (
                EngineTwo,
                glm::lerp(&glm::vec2(2., 3.), &glm::vec2(2.5, 2.5), t / 0.5),
                v,
            ),
            (t, v) if t < 1.5 => (
                EngineThree,
                glm::lerp(&glm::vec2(2.5, 2.5), &glm::vec2(4.5, 2.5), t - 0.5),
                v,
            ),
            (t, v) => (
                EngineTwo,
                glm::lerp(&glm::vec2(4.5, 2.5), &glm::vec2(7., 0.), (t - 1.5) / 2.5),
                v,
            ),
        };
    }
}
