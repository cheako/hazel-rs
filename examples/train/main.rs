//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use std::cell::RefCell;
use std::rc::Rc;

use hazel_ge::camera::{KeyMap as CameraMapping, Layer as Camera};
use hazel_ge::imgui;
use hazel_ge::reexport::slog::{info, trace};

mod axonometric;
mod data;
mod isometric;
struct InitLayer(Rc<RefCell<Camera>>);

impl hazel_ge::Layer<(), ()> for InitLayer {
    fn get_name(&mut self) -> String {
        "Train Initial".to_owned()
    }

    fn on_preupdate(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        api.layers[0]
            .iter()
            .position(|it| it.try_borrow_mut().is_err())
            .map(|e| api.layers[0].remove(e));

        let state = Rc::new(RefCell::new(data::State::default()));
        api.layers[0].push(state.clone());

        let layer = Rc::new(RefCell::new(isometric::ExampleLayer::new(
            api,
            self.0.clone(),
            state.clone(),
        )));
        api.layers[0].push(layer);

        let layer = Rc::new(RefCell::new(axonometric::ExampleLayer::new(
            api,
            self.0.clone(),
            state,
        )));
        api.layers[0].push(layer);
    }
}

#[derive(Default)]
struct ImguiInterface;

impl imgui::Interface for ImguiInterface {
    fn get_name(&mut self) -> String {
        "Sandbox Imgui Interface".to_owned()
    }

    fn on_update(&mut self, _ui: &hazel_ge::imgui::Ui) {}
}

fn main() {
    use hazel_ge::event::VirtualKeyCode;

    let mut app: hazel_ge::Application<(), ()> = Default::default();
    let imgui_interface = Rc::new(RefCell::new(ImguiInterface::default()));

    let innet_size = app.window.get_window().inner_size();
    let camera = Rc::new(RefCell::new(Camera::new(
        innet_size.width as f32 / innet_size.height as f32,
        CameraMapping {
            left: VirtualKeyCode::A,
            down: VirtualKeyCode::S,
            right: VirtualKeyCode::D,
            up: VirtualKeyCode::W,
            clockwise: Some((VirtualKeyCode::Q, VirtualKeyCode::E)),
        },
    )));

    app.layers.push(vec![
        Rc::new(RefCell::new(InitLayer(camera.clone()))),
        camera,
    ]);
    app.layers
        .push(vec![Rc::new(RefCell::new(imgui::InitLayer::new(
            imgui_interface,
            1,
        )))]);
    app.run()
}
