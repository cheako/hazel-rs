//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use hazel_ge::camera::Layer as Camera;
use hazel_ge::im_str;
use hazel_ge::reexport::glm::{self, Mat3};
use hazel_ge::renderer::r2d::Image;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;

const MAX_STATS: usize = 25;

pub struct ExampleLayer {
    state: Rc<RefCell<crate::data::State>>,
    camera: Rc<RefCell<Camera>>,
    rail_images: Image<()>,
    engine_images: Image<()>,
    wagons_images: Image<()>,
    stats: VecDeque<(usize, usize, Vec<usize>)>,
}

impl ExampleLayer {
    pub fn new(
        api: &mut hazel_ge::AppAPI<(), ()>,
        camera: Rc<RefCell<Camera>>,
        state: Rc<RefCell<crate::data::State>>,
    ) -> Self {
        Self {
            state,
            camera,
            rail_images: api
                .renderer
                .load_image("assets/textures/opengfx-0.5.5/maglev_tracks_snow.png")
                .expect("Can't open maglev_tracks_snow.png"),
            engine_images: api
                .renderer
                .load_image("assets/textures/opengfx-0.5.5/arctic_CS4000_28px.png")
                .expect("Can't open arctic_CS4000_28px.png"),
            wagons_images: api
                .renderer
                .load_image("assets/textures/opengfx-0.5.5/arctic_railwagons_28px.png")
                .expect("Can't open arctic_CS4000_28px.png"),
            stats: VecDeque::with_capacity(MAX_STATS),
        }
    }
}

const ZOOM_LVL_BASE: i32 = 4;
fn remap_coords(x: i32, y: i32, z: i32) -> [i32; 2] {
    [(y - x) * 2 * ZOOM_LVL_BASE, (y + x - z) * ZOOM_LVL_BASE]
}

impl hazel_ge::Layer<(), ()> for ExampleLayer {
    fn get_name(&mut self) -> String {
        "Train 2D Example Layer".to_owned()
    }

    fn on_imgui_update(&mut self, ui: &hazel_ge::imgui::Ui) {
        if let Some(w) = hazel_ge::reexport::imgui::Window::new(im_str!("2D Stats")).begin(&ui) {
            let images_pre = self.stats[0].0;
            let (_, images_removed, images_max, vertex_max) = self.stats.iter().fold(
                (images_pre, 0usize, 0usize, vec![]),
                |(images_pre, images_removed, images_max, vertex_max),
                 &(images_begin, images, ref vertex)| {
                    use std::cmp::{max, Ordering::*};
                    use std::iter::Sum;
                    (
                        images,
                        images_removed + images_pre - images_begin,
                        max(images_max, images),
                        match vertex.len().cmp(&vertex_max.len()) {
                            Greater => vertex.clone(),
                            Equal if usize::sum(vertex.iter()) > usize::sum(vertex_max.iter()) => {
                                vertex.clone()
                            }
                            _ => vertex_max,
                        },
                    )
                },
            );
            ui.text(im_str!("images_removed: {}", images_removed));
            ui.text(im_str!("images_max: {}", images_max));
            ui.text(im_str!("vertex_max: {:?}", vertex_max));
            w.end(&ui);
        }
    }

    fn on_update(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        api.renderer
            .begin_scene(self.camera.borrow_mut().camera.get_view_projection())
            .unwrap();
        {
            let renderer = &mut api.renderer.renderer_2d;
            let state = self.state.borrow();
            use hazel_ge::renderer::r2d::Component::*;
            const SCALE: f32 = 0.1;
            const IMAGE_SCALE: f32 = SCALE * 2.;
            const RAIL_TWO: Option<((u32, u32), (u32, u32))> = Some(((114, 13), (64, 31)));
            const RAIL_THREE: Option<((u32, u32), (u32, u32))> = Some(((194, 13), (64, 31)));
            const RAIL_FOUR: Option<((u32, u32), (u32, u32))> = Some(((274, 13), (64, 31)));
            state.map.iter().enumerate().for_each(|y| {
                y.1.iter().enumerate().for_each(|x| {
                    use crate::data::Tile::*;
                    if *x.1 == Empty {
                        return;
                    }
                    renderer
                        .draw(vec![
                            BlueIsAlpha,
                            Position(glm::vec2(
                                (x.0 as i32 - 4) as f32 * SCALE,
                                -(y.0 as i32) as f32 * SCALE,
                            )),
                            Size(glm::vec2(IMAGE_SCALE, IMAGE_SCALE)),
                            Image(
                                self.rail_images.clone(),
                                match x.1 {
                                    Empty => unreachable!(),
                                    SN => RAIL_TWO,
                                    NN => RAIL_THREE,
                                    SS => RAIL_FOUR,
                                },
                            ),
                        ])
                        .unwrap();
                })
            });
            const ENGINE_TWO: ((u32, u32), (u32, u32)) = ((19, 1), (20, 16));
            const ENGINE_THREE: ((u32, u32), (u32, u32)) = ((51, 1), (29, 12));
            const WAGON_FOOD_TWO: ((u32, u32), (u32, u32)) = ((94, 216), (20, 16));
            const WAGON_FOOD_THREE: ((u32, u32), (u32, u32)) = ((126, 216), (29, 12));
            state.objects.iter().for_each(|o| {
                use crate::data::Object::*;
                let (image, sub_image) = match o.0 {
                    EngineTwo => (self.engine_images.clone(), ENGINE_TWO),
                    EngineThree => (self.engine_images.clone(), ENGINE_THREE),
                    FoodTwo => (self.wagons_images.clone(), WAGON_FOOD_TWO),
                    FoodThree => (self.wagons_images.clone(), WAGON_FOOD_THREE),
                    PaperSix => (self.wagons_images.clone(), WAGON_FOOD_TWO),
                    PaperSeven => (self.wagons_images.clone(), WAGON_FOOD_THREE),
                };
                renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::vec2((o.1.x - 4.) * SCALE, o.1.y * -SCALE)),
                        Size(glm::vec2(
                            IMAGE_SCALE * (sub_image.1).0 as f32 / 64.,
                            IMAGE_SCALE * (sub_image.1).1 as f32 / 31.,
                        )),
                        Image(image, Some(sub_image)),
                    ])
                    .unwrap()
            });
            /* match state.t {
                t if t < 0.5 => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(-2. * SCALE, -3. * SCALE),
                            &glm::vec2(-1.5 * SCALE, -2.5 * SCALE),
                            t / 0.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 20. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_TWO),
                    ])
                    .unwrap(),
                t if t < 1.5 => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(-1.5 * SCALE, -2.5 * SCALE),
                            &glm::vec2(0.5 * SCALE, -2.5 * SCALE),
                            t - 0.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 29. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_THREE),
                    ])
                    .unwrap(),
                t => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(0.5 * SCALE, -2.5 * SCALE),
                            &glm::vec2(3. * SCALE, 0. * SCALE),
                            (t - 1.5) / 2.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 20. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_TWO),
                    ])
                    .unwrap(),
            }
            match state.t {
                t if t < 1.5 => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(-3. * SCALE, -4. * SCALE),
                            &glm::vec2(-1.5 * SCALE, -2.5 * SCALE),
                            t / 1.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 20. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), WAGON_FOOD_TWO),
                    ])
                    .unwrap(),
                t if t < 2.5 => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(-1.5 * SCALE, -2.5 * SCALE),
                            &glm::vec2(0.5 * SCALE, -2.5 * SCALE),
                            t - 1.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 29. / 64., IMAGE_SCALE * 12. / 31.)),
                        Image(self.engine_images.clone(), WAGON_FOOD_THREE),
                    ])
                    .unwrap(),
                t => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(0.5 * SCALE, -2.5 * SCALE),
                            &glm::vec2(2. * SCALE, -1. * SCALE),
                            (t - 2.5) / 1.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 20. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), WAGON_FOOD_TWO),
                    ])
                    .unwrap(),
            } */
            /* match self.timer.borrow().0 % 4. {
                t if t < 0.5 => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(-2. * SCALE, -3. * SCALE),
                            &glm::vec2(-1.5 * SCALE, -2.5 * SCALE),
                            t * 2.,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 20. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_TWO),
                    ])
                    .unwrap(),
                t if t < 1.5 => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(-1.5 * SCALE, -2.5 * SCALE),
                            &glm::vec2(0.5 * SCALE, -2.5 * SCALE),
                            t - 0.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 29. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_THREE),
                    ])
                    .unwrap(),
                t => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(0.5 * SCALE, -2.5 * SCALE),
                            &glm::vec2(3. * SCALE, 0. * SCALE),
                            (t - 1.5) / 2.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 20. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_TWO),
                    ])
                    .unwrap(),
            }
            match self.timer.borrow().0 % 4. {
                t if t < 0.5 => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(-2. * SCALE, -3. * SCALE),
                            &glm::vec2(-1.5 * SCALE, -2.5 * SCALE),
                            t * 2.,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 20. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_TWO),
                    ])
                    .unwrap(),
                t if t < 1.5 => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(-1.5 * SCALE, -2.5 * SCALE),
                            &glm::vec2(0.5 * SCALE, -2.5 * SCALE),
                            t - 0.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 29. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_THREE),
                    ])
                    .unwrap(),
                t => renderer
                    .draw(vec![
                        BlueIsAlpha,
                        Position(glm::lerp(
                            &glm::vec2(0.5 * SCALE, -2.5 * SCALE),
                            &glm::vec2(3. * SCALE, 0. * SCALE),
                            (t - 1.5) / 2.5,
                        )),
                        Size(glm::vec2(IMAGE_SCALE * 20. / 64., IMAGE_SCALE * 16. / 31.)),
                        Image(self.engine_images.clone(), ENGINE_TWO),
                    ])
                    .unwrap(),
            } */
        }

        api.renderer
            .end_scene()
            .into_iter()
            .collect::<hazel_ge::renderer::base::Result<()>>()
            .unwrap();

        let r2d = &api.renderer.renderer_2d;
        self.stats.push_front((
            r2d.stats_images_begin,
            r2d.stats_images,
            r2d.stats_vertex.clone(),
        ));
        self.stats.truncate(MAX_STATS);
    }
}
