//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use std::cell::RefCell;
use std::rc::Rc;

use ash_tray::vk_helper::CommandBuffer;
use hazel_ge::camera::{KeyMap as CameraMapping, Layer as Camera};
use hazel_ge::imgui;
use hazel_ge::reexport::slog::{info, trace};
use hazel_ge::renderer::VkHelperUser;

struct ExampleEventLayer {
    ctr: i64,
}

impl hazel_ge::Layer<(), ()> for ExampleEventLayer {
    fn get_name(&mut self) -> String {
        format!("Sandbox Example Event: ctr == {};", self.ctr)
    }

    fn on_update(
        &mut self,
        _api: &hazel_ge::AppAPI<(), ()>,
        _command_buffer: &CommandBuffer<VkHelperUser<()>>,
        _wt: &hazel_ge::reexport::winit::event_loop::EventLoopWindowTarget<()>,
        _c: &mut hazel_ge::reexport::winit::event_loop::ControlFlow,
    ) {
        // trace!(_api.log, "{}", "on_update");
    }

    fn on_event(
        &mut self,
        api: &hazel_ge::AppAPI<(), ()>,
        e: &hazel_ge::event::Event<()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) -> bool {
        use hazel_ge::event::{DeviceEvent::*, Event::*, WindowEvent::*};
        match e {
            NewEvents(hazel_ge::event::StartCause::Poll) => {
                self.ctr += 1;
                if self.ctr % 1000 == 0 {
                    info!(api.log, "ExampleLayer::Update");
                    self.ctr = 0;
                };
            }
            WindowEvent {
                event: CursorMoved { .. },
                ..
            } => {}
            WindowEvent {
                event: AxisMotion { .. },
                ..
            } => {}
            WindowEvent {
                event: Resized { .. },
                ..
            } => {}
            DeviceEvent {
                event: MouseMotion { .. },
                ..
            } => {}
            DeviceEvent {
                event: Motion { .. },
                ..
            } => {}
            MainEventsCleared => {}
            RedrawEventsCleared => {}
            _ => trace!(api.log, "{}", format!("{:?}", e)),
        }
        false
    }
}

mod s2d;
mod s3d;

struct InitLayer(Rc<RefCell<Camera>>);

impl hazel_ge::Layer<(), ()> for InitLayer {
    fn get_name(&mut self) -> String {
        "Sandbox Initial".to_owned()
    }

    fn on_preupdate(
        &mut self,
        api: &hazel_ge::AppAPI<(), ()>,
        command_buffer: &CommandBuffer<VkHelperUser<()>>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        let mut layers = api.layers.borrow_mut();
        layers[0]
            .iter()
            .position(|it| it.try_borrow_mut().is_err())
            .map(|e| layers[0].remove(e));

        let layer = Rc::new(RefCell::new(s3d::ExampleLayer::new(api, command_buffer,self.0.clone())));
        layers[0].push(layer);

        let layer = Rc::new(RefCell::new(s2d::ExampleLayer::new(api, command_buffer,self.0.clone())));
        layers[0].push(layer);
    }
}

#[derive(Default)]
struct ImguiInterface;

impl imgui::Interface for ImguiInterface {
    fn get_name(&mut self) -> String {
        "Sandbox Imgui Interface".to_owned()
    }

    fn on_update(&mut self, _ui: &hazel_ge::imgui::Ui) {}
}

fn main() {
    use hazel_ge::event::VirtualKeyCode;

    let mut app: hazel_ge::Application<(), ()> = Default::default();
    let imgui_interface = Rc::new(RefCell::new(ImguiInterface::default()));

    let innet_size = app.window.get_window().inner_size();
    let camera = Rc::new(RefCell::new(Camera::new(
        innet_size.width as f32 / innet_size.height as f32,
        CameraMapping {
            left: VirtualKeyCode::A,
            down: VirtualKeyCode::S,
            right: VirtualKeyCode::D,
            up: VirtualKeyCode::W,
            clockwise: Some((VirtualKeyCode::Q, VirtualKeyCode::E)),
        },
    )));

    app.layers.push(vec![
        // Rc::new(RefCell::new(ExampleEventLayer { ctr: 3 })),
        Rc::new(RefCell::new(InitLayer(camera.clone()))),
        camera,
    ]);
    app.layers
        .push(vec![Rc::new(RefCell::new(imgui::InitLayer::new(
            imgui_interface,
            1,
        )))]);
    app.run()
}
