//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use ash_tray::vk_helper::CommandBuffer;
use hazel_ge::camera::Layer as Camera;
use hazel_ge::reexport::glm::{self, Vec4};
use hazel_ge::renderer::r2d::Image;
use hazel_ge::renderer::VkHelperUser;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;

const MAX_STATS: usize = 25;

pub struct ExampleLayer {
    camera: Rc<RefCell<Camera>>,
    color: Vec4,
    images: [Image<()>; 2],
    stats: VecDeque<(usize, usize, Vec<usize>)>,
}

impl ExampleLayer {
    pub fn new(
        api: &hazel_ge::AppAPI<(), ()>,
        command_buffer: &CommandBuffer<VkHelperUser<()>>,
        camera: Rc<RefCell<Camera>>,
    ) -> Self {
        Self {
            camera,
            color: Vec4::new(0.2f32, 0.3, 0.8, 1.),
            images: [
                api.renderer
                    .load_image(command_buffer, "assets/textures/rock_03_h2.png")
                    .expect("Can't open rock_03_h2.png"),
                api.renderer
                    .load_image(command_buffer, "assets/textures/cheako_logo.png")
                    .expect("Can't open cheako_logo.png"),
            ],
            stats: VecDeque::with_capacity(MAX_STATS),
        }
    }
}

impl hazel_ge::Layer<(), ()> for ExampleLayer {
    fn get_name(&mut self) -> String {
        "Sandbox 2D Example Layer".to_owned()
    }

    fn on_imgui_update(&mut self, ui: &hazel_ge::imgui::Ui) {
        let color = self.color.as_mut_slice();
        let mut color_mut = [color[0], color[1], color[2], color[3]];
        ui.color_edit4("Square 2D Color", &mut color_mut);
        color[0] = color_mut[0];
        color[1] = color_mut[1];
        color[2] = color_mut[2];
        color[3] = color_mut[3];
        if let Some(w) = ui.window("2D Stats").begin() {
            let images_pre = self.stats[0].0;
            let (_, images_removed, images_max, vertex_max) = self.stats.iter().fold(
                (images_pre, 0usize, 0usize, vec![]),
                |(images_pre, images_removed, images_max, vertex_max),
                 &(images_begin, images, ref vertex)| {
                    use std::cmp::{max, Ordering::*};
                    use std::iter::Sum;
                    (
                        images,
                        images_removed + images_pre - images_begin,
                        max(images_max, images),
                        match vertex.len().cmp(&vertex_max.len()) {
                            Greater => vertex.clone(),
                            Equal if usize::sum(vertex.iter()) > usize::sum(vertex_max.iter()) => {
                                vertex.clone()
                            }
                            _ => vertex_max,
                        },
                    )
                },
            );
            ui.text(format!("images_removed: {}", images_removed));
            ui.text(format!("images_max: {}", images_max));
            ui.text(format!("vertex_max: {:?}", vertex_max));
            w.end();
        }
    }

    fn on_update(
        &mut self,
        api: &hazel_ge::AppAPI<(), ()>,
        command_buffer: &CommandBuffer<VkHelperUser<()>>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        const SCALE: f32 = 0.25;
        api.renderer
            .begin_scene(self.camera.borrow_mut().camera.get_view_projection())
            .unwrap();
        {
            let renderer = &api.renderer.renderer_2d;
            use hazel_ge::renderer::r2d::Component::*;
            renderer
                .draw(vec![
                    Position(glm::vec2(-2f32 * SCALE, -2. * SCALE)),
                    Size(glm::vec2(1.5f32 * SCALE, 1.5 * SCALE)),
                    Image(self.images[0].clone(), None),
                ])
                .unwrap();

            renderer
                .draw(vec![
                    Hex,
                    Position(glm::vec2(-0.5f32 * SCALE, -2. * SCALE)),
                    Size(glm::vec2(1.5f32 * SCALE, 1.5 * SCALE)),
                    Image(self.images[0].clone(), None),
                ])
                .unwrap();
        }
        for y in 0..20 {
            for x in 0..20 {
                let renderer = &api.renderer.renderer_2d;
                use hazel_ge::renderer::r2d::Component::*;
                if (x % 2 == 0) ^ (y % 2 == 0) {
                    renderer
                        .draw(vec![
                            Position(glm::vec2(x as f32 * 0.11 * SCALE, y as f32 * 0.11 * SCALE)),
                            Size(glm::vec2(0.1f32 * SCALE, 0.1 * SCALE)),
                            Color(self.color),
                        ])
                        .unwrap();
                } else {
                    renderer
                        .draw(vec![
                            Hex,
                            Position(glm::vec2(x as f32 * 0.11 * SCALE, y as f32 * 0.11 * SCALE)),
                            Size(glm::vec2(0.1f32 * SCALE, 0.1 * SCALE)),
                            Color(self.color),
                        ])
                        .unwrap();
                };
            }
        }

        {
            let renderer = &api.renderer.renderer_2d;
            use hazel_ge::renderer::r2d::Component::*;
            renderer
                .draw(vec![
                    Position(glm::vec2(-2f32 * SCALE, -2. * SCALE)),
                    Size(glm::vec2(1.5f32 * SCALE, 1.5 * SCALE)),
                    // https://www.h-schmidt.net/FloatConverter/IEEE754.html
                    Z(0.9999999),
                    Color(glm::vec4(0.4f32, 1., 1., 1.)),
                    Image(self.images[1].clone(), None),
                ])
                .unwrap();

            renderer
                .draw(vec![
                    Hex,
                    Position(glm::vec2(-0.5f32 * SCALE, -2. * SCALE)),
                    Size(glm::vec2(1.5f32 * SCALE, 1.5 * SCALE)),
                    Z(0.9999999),
                    Color(glm::vec4(0.4f32, 1., 1., 1.)),
                    Image(self.images[1].clone(), None),
                ])
                .unwrap();
        }
        api.renderer.end_scene(command_buffer).unwrap();

        let r2d = &api.renderer.renderer_2d;
        self.stats.push_front((
            r2d.stats_images_begin.get(),
            r2d.stats_images.get(),
            r2d.stats_vertex.borrow().clone(),
        ));
        self.stats.truncate(MAX_STATS);
    }
}
