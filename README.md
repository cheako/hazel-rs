# Hazel-rs
Hazel Game Engine, inspired by the [Hazel Game Engine](https://www.github.com/TheCherno/Hazel/).

## Testing
- Find your own guide for setting up Rust on your system in whatever format is best for you.

The example sandbox can be run with `cargo run --example sandbox`.  Hazel-rs is a Library Crate and has no "default" application to run.

## Using
Chances are you'll be submoduling this repo into your crate and using Cargo's `path` semantics to reference `hazel`.  The version(id) in the repo will be ahead of the published version for this purposes.
### git commands
##### References
Do not blindly pull from the Gitlab repo as it will not normally be in a consistent or even usable state.  Instead use `git fetch` and `git checkout <ref>` to access a specific reference.
##### Branches
Be-four you make local edits, create a branch for that edit with `git checkout -b <meaningful_identifier>`.  When it's time to bring in changes from upstream do so in your branch with a rebase, `git rebase <ref>` if your target ref is not found you may have to run fetch again.
##### Remotes
When you get to the point of making pull requests you'll likely want your repo as `origin` and [the parent](https://gitlab.com/cheako/hazel-rs) repo under another name.  This README shouldn't need to contain a git manual, but there was one complaint about not knowing how to navigate so here is my attempt at addressing some things.  If you're having problems I usually find they can be cleared up with a `git rebase -i --root`.  Try reading more about git and playing around.
##### History or selecting a good ref
Check out Gitlab's pipelines.

## To Do
- Define coordinate system, currently textures are drawn upside-down
- Fix key [state tracking](https://github.com/rust-windowing/winit/issues/753)
- Global(off window) mouse tracking
- See items for further investigation
- Make use of crate failure
- Smoth resize
- Todo in code

## Post 0.0.0
- Reorganize mod renderer

## Investigate
### Multiple Graphics APIs
The Hazel project has support for multiple APIs as a main feature AND regularly rejects optimizations that don't offer a substantial improvement.  I believe, hence the need for investigation, that these goals are in conflict.  Demonstrate a measurable, by any metric you choose, improvement over ash/Vulkan.

I believe that the notion of a difference in APIs is antiquated OR coming from a *position of compromise. *Like choosing to use closed source drivers and then complaining that the provided drivers are inadequate, switching to FOSS drivers and fixing support on your own is the obvious solution.

## Non-actionable
### Multiple windowing APIs
It's likely that any alternative will be different enough that providing support for both would result in a negative situation.
