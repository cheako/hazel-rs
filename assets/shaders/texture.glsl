// Basic Texture Shader

#type vertex
#version 330 core
#extension GL_ARB_separate_shader_objects:enable
#extension GL_ARB_shading_language_420pack:enable

layout(location=0)in vec3 a_Position;
layout(location=1)in vec2 a_TexCoord;

layout(set=0,binding=0)uniform u_ViewProjection{mat4 ViewProjection;}u0;
layout(push_constant)uniform u_Transform{mat4 Transform;}u1;

layout(location=0)out vec2 v_TexCoord;

void main()
{
    v_TexCoord=a_TexCoord;
    gl_Position=u0.ViewProjection*u1.Transform*vec4(a_Position,1.);
}

#type fragment
#version 330 core
#extension GL_ARB_separate_shader_objects:enable
#extension GL_ARB_shading_language_420pack:enable

layout(location=0)out vec4 color;

layout(location=0)in vec2 v_TexCoord;

layout(set=1,binding=0)uniform sampler2D Texture;

void main()
{
    color=texture(Texture,v_TexCoord);
}
