// Flat Color Shader

#type vertex
#version 330 core
#extension GL_ARB_separate_shader_objects:enable
#extension GL_ARB_shading_language_420pack:enable

layout(location=0)in vec3 a_Position;

layout(set=0,binding=0)uniform u_ViewProjection{mat4 ViewProjection;}u0;
layout(push_constant)uniform u_Transform{
    layout(offset=16)mat4 Transform;
}u1;

void main()
{
    gl_Position=u0.ViewProjection*u1.Transform*vec4(a_Position,1.);
}

#type fragment
#version 330 core
#extension GL_ARB_separate_shader_objects:enable
#extension GL_ARB_shading_language_420pack:enable

layout(location=0)out vec4 color;

layout(push_constant)uniform u_Color{
    vec4 Color;
}u;

void main()
{
    color=u.Color;
}
